#  General Imports
from __future__ import division, print_function
import numpy as np
import os
import sys
import shutil
from scipy.constants import e, c, m_p
from scipy.interpolate import  PchipInterpolator
#  BLonD Imports
try:
    import blond
except ImportError:
    raise ImportError("BLonD is not installed. Please install BLonD and make sure in can be found in your current Python Environment. BLonD can be found at https://gitlab.cern.ch/blond/blond")
from blond.input_parameters.ring import Ring
from blond.input_parameters.rf_parameters import RFStation
from blond.trackers.tracker import RingAndRFTracker
from blond.trackers.tracker import FullRingAndRF
from blond.beam.beam import  Proton
from blond.beam.profile import CutOptions, Profile
from Gaussian_optimizer.blond_additions.blond_beam import CustomBeam
from Gaussian_optimizer.blond_additions.blond_monitors import MultiBunchMonitor, LossesMonitor
from Gaussian_optimizer.blond_additions.blond_comoving_profile import ComovingProfile
from Gaussian_optimizer.blond_additions.blond_PS_llrf import BeamFeedback_PS
from Gaussian_optimizer.blond_additions.blond_plot import Plot
from blond.utils import bmath as bm
from blond.impedances.impedance import InducedVoltageFreq, TotalInducedVoltage, InductiveImpedance
from blond.beam.distributions_multibunch import matched_from_line_density_multibunch

# Please make sure to have the PS Impedance model is your PythonPath
# It can be found at https://gitlab.cern.ch/longitudinal-impedance/PS/
# sys.path.insert(0, 'C:/Users/onaumenk/cernbox/Documents/PythonProjects/PSImpedance')
try:
    from impedance_loader import ImpedanceLoader
    from impedance_toolbox.impedance_toolbox.machine_params import MachineParameters
except ImportError:
    try:
        from PSImpedance.impedance_loader import ImpedanceLoader
        from PSImpedance.impedance_toolbox.impedance_toolbox.machine_params import MachineParameters
    except ImportError:
        raise ImportError("Please make sure to have the PS Impedance model is your PythonPath. It can be found at https://gitlab.cern.ch/longitudinal-impedance/PS/")
    

import matplotlib.pyplot as plt

np.finfo(np.dtype("float32"))
np.finfo(np.dtype("float64"))

"""
Script to run a simulation for the BigTofEast cycle between
726 and 735 ms cycle time, i.e. during the 1st and 2nd phase jump
performed for Tof extraction and counter-rotation, respectively.

Please make sure to have the PS Impedance model is your PythonPath
It can be found at https://gitlab.cern.ch/longitudinal-impedance/PS/

Author: **Oleksandr Naumenko**
"""
this_directory = os.path.dirname(os.path.realpath(__file__))
above_pckg_dir = os.path.realpath(os.path.join(this_directory, "..", ".."))
def time_to_turns(time, PS_turn = 2.09e-6):
    return int(np.round(time/PS_turn))

def run(intensity_effects = True, phase_jump = True, feedback_loops = True, extraction_phase_glitch_correction = True,
        model = "C10+C20+C40+C80+C200+finemet+kickers+misc", delete_folder = True, monitor_losses= True,
        monitor_time_distribution = True,

        # Times after 726 ms
        N_t_equilibrium = 100,
        N_t_before_jump = time_to_turns(0.1e-3)+200, N_t_j1= time_to_turns(0.4649e-3), N_t_m= time_to_turns(2.8351e-3), 
        N_t_j2 = time_to_turns(0.4e-3), N_t_after = 3000, N_t_off = 1, N_t_extraction = time_to_turns(2e-3) + 200, #200 turn offset in the cycle to allow the 1st phase jump start time to vary more
        dt_plt_on = 100, dt_plt_phase_jump = 10, dt_plt_off = 100,

        output_dir ='output_files/Test',
        line_density_type_TOF = "binomial", line_density_exponent_TOF = 0.778294, bunch_length_TOF = 47.4348e-9,
        line_density_type_EAST = "binomial", line_density_exponent_EAST = 5, bunch_length_EAST = 84.173e-9,
        bunch_spacing_buckets = 4,

        start_time = 726, # in ms
        use_start_momentum = True,  
        momentum_ramp_data_path = os.path.join(above_pckg_dir, 'data/PS_EASTBIGTOF_Momentum.csv'),
        accel_rate_cut = 8.38e3,

        slices_per_bucket= 2000,
        N_rf_sections = 1, N_macroparticles = int(1e6), N_10MHz_cavities= 1, N_200MHz_cavities = 0,
        n_turns_wake = 1, n_turns_FRONT_wake = 1, max_frequency = 200e6,

        voltage=200e3, intensities=[355e10,29e10],  # Adjust, [TOF_intensity, EAST_intensity]
        pj1_amp_to = 2*np.pi*(152/360), pj1_amp_back = 2*np.pi*(176/360),
        pj2_amp_to = np.pi, pj2_amp_back = np.pi):
    """

   Args:
       intensity_effects: bool,
            Include intensity effects? True or False
       phase_jump: bool,
            Include phase jump? True or False
       feedback_loops: bool
            Include LLRF feedback loops, i.e. phase and radial loops of the PS? True or False
        extraction_phase_glitch_correction: bool
            If True, will correct the phase glitch of the phase loop at TOF extraction by offsetting the phase loop target phase by the difference between east bunch and full beam phase
       model: str,
            String specifying the impedance model
            May contain:
            "C10"
            "C20"
            "C40"
            "C80"
            "C200"
            "Finemet"
            "Kickers"
            "Misc"
            "Feedback"
            corresponding to the PS components contributing to the impedance. The "Feedback" handle imports all cavities
            with impedance reducing 1TFB or MHFB
       delete_folder: bool,
            Delete previous output? True or False

        monitor_time_distribution: bool
           Monitor the bunch profiles?
           If True, will create a h5py file in the output directory named "profile.h5"
           The structure of the h5py file is:
           'default'
           |--- 'profile'  ==>  Bunch Profiles
           |--- 'bin_centers'  ==> Center of each bin (i.e. the dt coordinates for the bunch profile values)
           |--- 'bin_centers_phase' ==> Same as bin centers, but in phase coordinates
           |--- 'turns' ==> Turn of the measurement
           Additionally, if only one bunch is present, then this file will contain
           |--- 'mean_dE' ==> Mean dE value
           |--- 'dE_norm' ==>
           |--- 'mean_dt'==> Mean dt value
           |--- 'dt_norm'
           |--- 'std_dE' ==> Standard Deviation of beam dE
           |--- 'std_dt' ==> Standard Deviation of beam dt

        monitor_losses: bool,
           Monitor losses for East and Tof bunches (seperately)?
           If True, will create a h5py file in the output directory named "losses.h5"
           This file will contain keys "tof_losses", "east_losses", "total_losses", showing the relative losses
           of the TOF (relative to total number of particles in TOF at the start)
           and EAST (also relative to the particles in EAST at the start of simulation), as well as the total relative losses

        N_t_equilibrium: int
            Turns at the start of the simulation where the RF parameters remain constant at their start values.
            Enables the RF loops, if active, to reach an equilibrium between themselves before starting the phase jump.
            To reach perfect equilibrium, about 60'000 turns are necessary.
            At minimum, 100 turns are needed for the loops to stop being chaotic at the start of the simulation

        N_t_before_jump: int
            Turns before the first phase jump.

        N_t_j1: int
            Duration of the first phase jump in turns

        N_t_m: int
            Duration of EAST rotation, before initiating the 2nd phase jump, in turns,

        N_t_j2: int
            Duration of the second phase jump in turns

        N_t_off: int
            Duration of simulation after both phase jumps have been performed

        N_t_extraction: int
            Turn at which TOF is extracted, i.e. removed from the simulation

        dt_plt_on : int
            Interval between 2 plots while the RF is on and no phase jump is being performed, in turns

        dt_plt_phase_jump: int
            Interval between 2 plots while a phase jump is being performed, in turns

        dt_plt_off: int
            Interval between 2 plots after RF is off, in turns

        output_dir : str
            Directory in which to store the results

        line_density_type_TOF: str
            Function to use for the line density that is used to construct the initial particle distribution for the TOF bunch.
            "Gaussian" or "Binomial" possible

        line_density_exponent_TOF: double
            Corresponds to (mu-0.5), where mu is the total exponent in the binomial line density  for the TOF bunch
        bunch_length_TOF: double
            Full bunch length of TOF used for binomial line density

        bunch_length_TOF: double
            Full bunch length of TOF used for binomial line density

        line_density_type_EAST: str
            Function to use for the line density that is used to construct the initial particle distribution for the EAST bunch.
            "Gaussian" or "Binomial" possible

        line_density_exponent_EAST: double
            Corresponds to (mu-0.5), where mu is the total exponent in the binomial line density  for the EAST bunch

        bunch_length_EAST: double
            Full bunch length of EAST used for binomial line density

        bunch_spacing_buckets: int
            Bunch spacing between TOF and EAST bunch. TOf is always placed in the 1st bucket, and EAST is placed in
            the bucket determined by this value. The bucket of EAST is simply 1 + bunch_spacing_buckets

        start_time: double
            Turn 0 of simulation corresponds to start_time in the machine.
            Given in ms
            Used to import momentum program for the simulation

       use_start_momentum: bool
            If True, will use the momentum value at start_time for the entirety of the simulation
            Used to stop small fluctuations in the momentum program from negatively impacting the simulation
            (e.g. to allow the loops to establish a new equilibrium without additional fluctuations)

        momentum_ramp_data_path: str
            Path to momentum program as .csv file, exported from LSA.
            Will use data starting at start_time, in ms.
        accel_rate_cut: positive float
            If > 0, will treat the losses as if the particles were accelerating at the ramp rate given by accel_rate_cut (in eV)
            for the last 1000 turns where RF is on.
            Used to gain an approximation of the particles lost in EAST after reacceleration in PS BIGTOFEAST


        slices_per_bucket: int
            Amount of slices per bucket

        N_rf_sections: int
            Number of RF sections. Should principally be 1, as effects of multiple sections
            are simply added up.

        N_macroparticles: int
            Number of macroparticles.

        N_10MHz_cavities: int
            Number of active/non-short circuted 10 MHz cavities of the PS used for the simulation

        N_200MHz_cavities: int
            Number of 200 MHz PS cavities that are active in the simulation

        n_turns_wake: int
            Number of turns over which the wake is saved.
        
        n_turns_FRONT_wake: int
            Number of turns used to generate the front padding of the wake data. 
            Used if the bunches move due to e.g. frequency shifts, to prevent them from moving out of the
            front edge of the wake data/x-axis.
            If using beam feedbacks, should be > 0 to be safe.

        max_frequency: double
            Frequency up to which the impedance is resolved, in Hz.

        voltage: double
            Total voltage of the principal, 10 MHz cavities.

        intensities: 2-tuple of doubles
            Intensities of TOF and EAST bunch, given as (TOF_intensity, EAST_intensity)

       pj1_amp_to: double
            Phase jump amplitude of the first jump when jumping TO the unstable phase. In radian

       pj1_amp_back: double
            Phase jump amplitude of the first jump when jumping BACK from the unstable phase. In radian

       pj2_amp_to: double
            Phase jump amplitude of the second jump when jumping TO the unstable phase. In radian

       pj2_amp_back: double
            Phase jump amplitude of the second jump when jumping BACK from the unstable phase. In radian

    """
    args = locals()
    argname = list(args.keys())
    argval = list(args.values())


    # Plot and Data Output directory
    this_directory = os.path.dirname(os.path.realpath(__file__))
    relative_directory = False
    if relative_directory:
        output_dir = os.path.join(this_directory, output_dir)

    # Tracking details
    # RF on times
    N_t_equilibrium = N_t_equilibrium
    N_t =  int(N_t_before_jump +  N_t_j1 + N_t_m+ N_t_j2 + N_t_after +  N_t_off)            # Total amount of turns tracked
    N_t_on = int(N_t_before_jump +  N_t_j1 + N_t_m+ N_t_j2 + N_t_after)   # Amount of turns the RF is on

    ### Import and Output Setup

    # Delete old output before writing anew
    if os.path.exists(output_dir) and delete_folder:
        shutil.rmtree(output_dir)
    os.makedirs(output_dir, exist_ok=True)

    # Write used arguments for reference
    lines = []
    for i in range(len(argname)):
        lines.append(argname[i] + ": " + str(argval[i]))
    with open(os.path.join(output_dir, "arguments.txt"), 'w') as f:
        f.write('\n'.join(lines))

    ### PS Parameters
    N_p = N_macroparticles   # Number of macroparticles
    C= 628.3 # Machine Circumference [m]


    turn_duration = 2.098e-3 # Turn duration in ms,
    # Imported Momentum Data of Cycle
    momentum_ramp_data_path = momentum_ramp_data_path
    momentum_data = np.genfromtxt(momentum_ramp_data_path, delimiter=",", skip_header=2).transpose()
    injection_time = momentum_data[0]
    momentum = momentum_data[1]*1e9

    index_start_momentum = np.nanargmin((np.abs(injection_time - start_time)))
    index_end_momentum = np.nanargmin((np.abs(injection_time - (start_time+ turn_duration*N_t))))
    time_interp = np.linspace(injection_time[index_start_momentum], injection_time[index_end_momentum], N_t_on)
    # Higher Order Interpolation
    interp = PchipInterpolator(injection_time[index_start_momentum:index_end_momentum], momentum[index_start_momentum:index_end_momentum])
    p_0 = interp(time_interp)
    # Former linear interpolation
    #p_0 = np.interp(time_interp, injection_time[index_start_momentum:index_end_momentum], momentum[index_start_momentum:index_end_momentum]) #*0 + momentum[index_start_momentum]
    if N_t_off > 0:
        p_off = np.ones(N_t_off + 1)*p_0[-1]
        p_0 = np.append(p_0,p_off)
    if use_start_momentum:
        p_0 = np.ones(N_t+1)*momentum[index_start_momentum]
    if N_t_equilibrium > 0:
        # match derivatives of momentum between equilibrium momentum and actual programmed momentum
        p_equilibrium = np.array([p_0[0]-i*(np.average(np.diff(p_0)[0:int(len(p_0)*0.01)])) for i in range(N_t_equilibrium)])
        p_equilibrium = np.flip(p_equilibrium)
        p_0 = np.insert(p_0,0,p_equilibrium)
        N_t_on = N_t_on + N_t_equilibrium
        N_t = N_t + N_t_equilibrium
        N_t_before_jump = N_t_before_jump + N_t_equilibrium
        N_t_extraction = N_t_extraction + N_t_equilibrium

    h = 8            # Harmonic number
    V = voltage                # RF voltage [V]
    gammaT = 6.272889 # Transition gamma
    alpha = 1./gammaT/gammaT        # First order mom. comp. factor
    E0 = m_p * c**2
    E = np.sqrt(p_0**2 + E0)
    beta = p_0/E
    wRev = beta*c/(C/(2*np.pi))



    ### Simulation setup

    ring = Ring(C, alpha, p_0, Proton(), N_t, bending_radius = 70.79)
    beam = CustomBeam(ring,N_p, np.sum(intensities))
    voltage = np.concatenate((np.ones(N_t_on)*V, np.zeros(N_t_off+1)))

    harmonics = np.repeat([np.repeat(h,N_t + 1, axis = 0)], N_10MHz_cavities, axis = 0)
    voltages = np.repeat([voltage], N_10MHz_cavities, axis = 0) / N_10MHz_cavities

    # Phase Jump Parameters
    phi_rf_d = np.zeros(N_t + 1)
    if phase_jump:
        phi_rf_d[N_t_before_jump:] = -pj1_amp_to
        phi_rf_d[(N_t_before_jump + N_t_j1 + 1):] = (pj1_amp_back-pj1_amp_to)

        N_t_j2_start = int(N_t_before_jump + N_t_j1 + N_t_m)
        phi_rf_d[N_t_j2_start:] = -pj2_amp_to
        phi_rf_d[(N_t_j2_start + N_t_j2 + 1):] = (pj2_amp_back - pj2_amp_to)

        phi_target_offset = np.copy(phi_rf_d)

    phi_rf_d = np.repeat([phi_rf_d], N_10MHz_cavities + N_200MHz_cavities, axis = 0)

    if N_200MHz_cavities > 0:
        h_200MHz = 32
        V_200MHz = 20e3 * 0
        voltage_200MHz = np.concatenate((np.ones(N_t_on) * V_200MHz, np.zeros(N_t_off + 1)))
        voltages_200MHz = np.repeat([voltage_200MHz], N_200MHz_cavities, axis = 0) / N_200MHz_cavities

        harmonics_200MHz = np.repeat([np.repeat(h_200MHz,N_t + 1, axis = 0)], N_200MHz_cavities, axis = 0)
        voltages = np.concatenate((voltages,voltages_200MHz))
        harmonics = np.concatenate((harmonics,harmonics_200MHz))


    rf = RFStation(ring, harmonics, voltages,  phi_rf_d = phi_rf_d, n_rf = N_10MHz_cavities+ N_200MHz_cavities,)

    # Profile and Slices
    N_slice = slices_per_bucket * 2 + slices_per_bucket * h * n_turns_wake
    bucket_length = C/(h*ring.beta*c)[0]
    cut_left = 0 - 2*bucket_length[0]
    cut_right = 0 + bucket_length[0] * h* n_turns_wake
    cut_opt = CutOptions(cut_left=cut_left, cut_right=cut_right, n_slices=N_slice)
    profile_sim = Profile(beam, CutOptions=cut_opt)

    cut_opt_obs = CutOptions(cut_left, cut_right, N_slice)
    cut_opt_east = CutOptions(cut_left=bunch_spacing_buckets * bucket_length[0], cut_right=cut_right,
                              n_slices=int(N_slice * (1 - bunch_spacing_buckets / h)))
    if feedback_loops is True:
        # Profile used for plotting, which moves alongside the change in instantaneous RF frequency.
        # This mimics the measurements using the RF clock as a turn reference, producing plots that show the
        # beam as a measurement would
        profile_obs = ComovingProfile(beam, rf, CutOptions = cut_opt_obs)
        slice_east = ComovingProfile(beam, rf, CutOptions=cut_opt_east)
    else:
        profile_obs = Profile(beam, CutOptions = cut_opt_obs)
        slice_east = Profile(beam, CutOptions=cut_opt_east)



    # Impedance/Beam Loading
    if intensity_effects:
        bunchParameters = {'Binomial exponent': [line_density_exponent_TOF, line_density_exponent_EAST], 'Bunch length [ns]': [bunch_length_TOF, bunch_length_EAST],
                           'Intensity per bunch [1e10 ppb]': [intensities[0], intensities[1]]}
        fillingScheme = {'Number of bunches': 2, 'Number of batches': 1, 'Number of fills': 1,
                         "Buckets between bunches": bunch_spacing_buckets, "Buckets between batches": 1, "Buckets between fills": 1}
        # Buckets between bunches includes the bucket of the bunch itself, i.e.
        # 3 empty buckets = 4 buckets spacing

        machineParams = MachineParameters(generalParameters=ring, rfParameters=rf,
                                          bunchParameters=bunchParameters, fillingScheme=fillingScheme,
                                          blondImport=True)
        machineParams.generateBeamCurrent(1, maxFreq=max_frequency)
        machineParams.generateBeamSpectrum()

        frequency_step = None  # ring.f_rev[0] / n_turns_wake
        if n_turns_wake == 1:
            multi_turn_wake = False
            front_wake_length = 0
        else:
            multi_turn_wake = True
            front_wake_length = n_turns_FRONT_wake / ring.f_rev[0]

        PS_loader = ImpedanceLoader(f_rev=ring.f_rev[0], momentum=p_0[0], freq_array=machineParams.freqArray)
        # C10
        if ("feedback" or "Feedback") in model:
            if "C10" in model:
                PS_loader.importC10_1TFB(n_elements=[10],
                                            main_harmonic=[h], impedance_reduction_target= [-15],
                                            main_harmonic_enableFB = [True])
                PS_loader.importC10_ClosedGapRelay(n_elements=1)
            if "C20" in model:
                # # C20 MHFB
                PS_loader.importC20_MHFB(n_elements=2)
            if "C40" in model:
                PS_loader.importC40_MHFB(n_elements=2)
            if "C80" in model:
                PS_loader.importC80_MHFB(n_elements=3)

        else:
            if "C10" in model:
                PS_loader.importC10(n_elements=10,
                                method='rf_cavities/C10/Individual/C10-11/Resonators/multi_resonators_h8.txt')
                PS_loader.importC10_ClosedGapRelay(n_elements=1)
            if "C20" in model:
                # C20
                PS_loader.importC20(n_elements=2)

            if "C40" in model:
                PS_loader.importC40(n_elements = 2)
            if "C80" in model:
                PS_loader.importC80(n_elements = 3)
        if "C200" in model:
            filename_C200 = '/rf_cavities/C200/Resonators/from_CST_C200_single_damped.txt'
            PS_loader.importC200(filename=filename_C200, n_elements=6)


        if ("finemet" or "Finemet") in model:
            PS_loader.importFinemet()


        if ("Kickers" or "kickers") in model:
            PS_loader.importKickers()  # filename_list=filename_Kickers

        if "misc" in model:
            # # Septa
            PS_loader.importSepta()

            # # Sector valves
            PS_loader.importSectorValves()

            # # MU upstream
            PS_loader.importMU_upstream()

            # # MU downstream
            PS_loader.importMU_downstream()

            # # Flanges
            PS_loader.importFlanges()

            # # Step transitions
            PS_loader.importSteps()

            # # Flanges ground loops
            PS_loader.importFlanges_GroundLoops()

            # # Beam instrumentation
            PS_loader.importBI()

            # # Misc equipment
            PS_loader.importMisc()

            # # Resistive wall
            PS_loader.importResistiveWall()



        imp = PS_loader.export2BLonD()

        ResonatorsList = imp.wakeList

        ImpedanceTable_list = imp.impedanceList
        ImZ_over_f_list = imp.ImZ_over_f_List

        impedance_cav = InducedVoltageFreq(beam, profile_sim, ResonatorsList,
                                           frequency_step, use_regular_fft=False,
                                           multi_turn_wake=multi_turn_wake,
                                           front_wake_length=front_wake_length,
                                           RFParams=rf)
        impedance_rest = InducedVoltageFreq(beam, profile_sim, ImpedanceTable_list,
                                            frequency_step, use_regular_fft=False,
                                            multi_turn_wake=multi_turn_wake,
                                            front_wake_length=front_wake_length,
                                            RFParams=rf)

        if max_frequency:
            max_index = (np.abs(impedance_cav.freq - max_frequency)).argmin()
            impedance_cav.total_impedance[max_index:] = 0

            max_index = (np.abs(impedance_rest.freq - max_frequency)).argmin()
            impedance_rest.total_impedance[max_index:] = 0
        # Space charge
        ImZ_over_n_impedance = []
        for ImZ in ImZ_over_f_list:
            ImZ_over_n = ImZ/rf.t_rev
            ImZ_over_n_impedance.append(InductiveImpedance(Beam=beam, Profile=profile_sim, Z_over_n=ImZ_over_n,
                                                           RFParams=rf, deriv_mode='gradient'))


       
        total_induced_voltage = TotalInducedVoltage(beam, profile_sim, [impedance_cav, impedance_rest] + ImZ_over_n_impedance)

    else:
        total_induced_voltage= None

    # Define tracker:
    # Can be passed external objects to track feedbacks
    # and use different solvers.
    # The tracker itself changes the beam parameters directly
    rf_section_trackers = []
    rf_section_trackers.append(RingAndRFTracker(rf, beam, solver = "exact",
                                                Profile=profile_sim))

    full_tracker = FullRingAndRF(rf_section_trackers)

    TOF_bunch_params = {"bunch_length" : bunch_length_TOF, "type" : line_density_type_TOF, "exponent": line_density_exponent_TOF}
    EAST_bunch_params = {"bunch_length": bunch_length_EAST, "type": line_density_type_EAST,
                        "exponent": line_density_exponent_EAST}

        
    matched_from_line_density_multibunch(beam, ring, full_tracker, [TOF_bunch_params, EAST_bunch_params],
                                             n_bunches=2,
                                             bunch_spacing_buckets=bunch_spacing_buckets, intensity_list=intensities,
                                             TotalInducedVoltage=total_induced_voltage)

    profile_sim.track()
    profile_obs.track()
    slice_east.track()
    if feedback_loops:

        if phase_jump:
            phase_target_offset = phi_target_offset
        else:
            phase_target_offset = 0

        loops_object = BeamFeedback_PS(ring, rf, profile_sim,
                                    PhaseNoise=None,
                                    phase_target_offset  = phase_target_offset, radial_reference=0,
                                    PL_gain=0.01924*wRev[0]/(2*np.pi), RL_gain=155.05*wRev[0]/(2*np.pi*ring.bending_radius),
                                    gd_pl=5.704, gi_pl=1 - 8.66e-5, g_rl= 0.993671)
    else:
        loops_object = None



    map_ = [profile_sim, profile_obs] + [full_tracker]
    if intensity_effects:
        map_ = map_ + [profile_sim] + [total_induced_voltage]
    if feedback_loops:
        map_ = map_ + [loops_object]

    # Now the part that saves our stuff
    if monitor_time_distribution:
        slices_monitor = MultiBunchMonitor(output_dir + '/profile_data', N_t, profile_obs, rf, 2)
        map_ = map_ + [slices_monitor]



    if monitor_losses:
        losses_monitor = LossesMonitor(output_dir + '/losses', N_t, ring, rf, beam, 2 * bucket_length[0],
                                       [N_macroparticles*intensities[0]/(np.sum(intensities)), N_macroparticles*intensities[1]/(np.sum(intensities))]
                                       ,N_t_extraction, beam_feedback = loops_object, 
                                       N_t_equilibrium=N_t_equilibrium, N_t_accel_losses= N_t_on - 1000, delta_E_accel=accel_rate_cut) #Treat losses as if they were in an accelerating bucket for the last synchrotron period
        map_ = map_ + [losses_monitor]

    # And plots our stuff:
    # Format options for our plot. Here we just specify plot output
    format_options_tof = {'dirname': output_dir + '/TOFfigs', 'alpha' : 10 ** (-np.log10(beam.n_macroparticles) / 6)}
    format_options_east = {'dirname': output_dir + '/EASTfigs', 'alpha': 10 ** (-np.log10(beam.n_macroparticles * intensities[1] / intensities[0]) / 10)}
    plots_tof = Plot(ring, rf, beam, dt_plt_on, N_t, bucket_length[0]*0.5 - bucket_length[0] * 1.1, bucket_length[0]*0.5 + bucket_length[0] * 1.1, -p_0[0] / 1e2, +p_0[0] / 1e2,
                     xunit='s', separatrix_plot=True, 
                     format_options=format_options_tof, Profile = profile_obs, PhaseLoop=loops_object,
                     delta_E_shown = accel_rate_cut)
    plots_east = Plot(ring, rf, beam, dt_plt_on, N_t, bucket_length[0]*4.5 - bucket_length[0] * 2, bucket_length[0]*4.5 + bucket_length[0] * 2, -p_0[0] / 1e2, +p_0[0] / 1e2,
                      xunit='s', separatrix_plot=True,
                      format_options=format_options_east, Profile = profile_obs, PhaseLoop=loops_object,
                      delta_E_shown = accel_rate_cut)
    map_ = map_ + [plots_tof] + [plots_east]



    # The tracking is done by iterating through all created objects:
    print("Map set")
    print("")

    # Tracking --------------------------------------------------------------------
    if feedback_loops is True:
        radial_offset = []
        phase_offset = []
        beam_phase_east = []
        beam_phase = []

    turns = np.arange(1, N_t_on + 1)
    for i in range(1, N_t_on + 1):
        if feedback_loops is True:
            # Track/Plot feedback behaviour
            phase_offset.append((loops_object.dphi))
            radial_offset.append(loops_object.drho)
            beam_phase.append(loops_object.beam_phase())
            beam_phase_east.append(loops_object.beam_phase_east(3*bucket_length[0]))
        
        if phase_jump is True:
            # Change plotting invervals for the duration of the 2 phase jumps
            phase_jump_intervals = [[int(N_t_before_jump),int(N_t_before_jump+ N_t_j1+1)], ] # Turns in which the phase jump is active
            phase_jump_intervals.append([int(N_t_j2_start), int(N_t_j2_start+N_t_j2+1)])
            idx_list = [j for j, interval in enumerate(phase_jump_intervals) if interval[0] <= i <= interval[1]]
            if idx_list:
                plots_tof.dt_plot = dt_plt_phase_jump
                plots_east.dt_plot = dt_plt_phase_jump
                full_tracker.dt_print = dt_plt_phase_jump
            else:
                plots_tof.dt_plot = dt_plt_on
                plots_east.dt_plot = dt_plt_on
                full_tracker.dt_print = dt_plt_on

        # TOF beam extraction
        if i == N_t_extraction:
            if feedback_loops is True and extraction_phase_glitch_correction is True:
                # Correct for the phase glitch at extraction of TOF by offsetting the phase loop target by the difference between measured full beam phase before extraction,
                # and the phase of only the east bunch before extraction (which is the same as the phase of the full beam after extraction)
                idx = rf.counter[0] + 1
                dc_phaseshift = loops_object.beam_phase_east(3*bucket_length[0]) - \
                                     loops_object.beam_phase()
                loops_object.phi_target_offset[idx:] += dc_phaseshift
                np.savetxt(output_dir + "/dc_phaseshift.txt", np.array([dc_phaseshift]))
            beam.losses_longitudinal_cut(2*bucket_length[0], np.max(beam.dt))
            beam.eliminate_lost_particles()
            plots_tof.dynamic_x_axis = None
            plots_tof.xmin = -bucket_length[0]
            plots_tof.xmax = bucket_length[0]


        if (i % plots_tof.dt_plot) == 0:
            print("Outputting at time step %d..." % i)
            print("   Beam momentum %.6e eV" % beam.momentum)
            print("   Beam gamma %3.3f" % beam.gamma)
            print("   Beam beta %3.3f" % beam.beta)
            print("   Beam energy %.6e eV" % beam.energy)
            print("   Four-times r.m.s. bunch length %.4e s" % (4.*beam.sigma_dt))
            print(" ")
        # Track
        for m in map_:
            m.track()
    if feedback_loops is True:
        plt.plot(turns[1:], phase_offset[1:])
        plt.ylabel("Phase offset")
        plt.xlabel("turns")
        plt.savefig(output_dir + "/seen_phase_offset.png")
        np.save(output_dir + "/seen_phase_offset.npy", np.array([turns, phase_offset]))
        plt.clf()

        plt.plot(turns[1:], radial_offset[1:])
        plt.ylabel("Radial offset")
        plt.xlabel("turns")
        plt.savefig(output_dir + "/rad_offset.png")
        np.save(output_dir + "/rad_offset.npy", np.array([turns, radial_offset]))
        plt.clf()

        plt.plot(np.arange(0,N_t_on,1),rf.omega_rf[0,0:N_t_on], label = "Actual")
        plt.plot(np.arange(0,N_t_on,1),rf.omega_rf_d[0,0:N_t_on], label = "Design")
        plt.legend()
        plt.ylabel("RF Frequency")
        plt.xlabel("turns")
        plt.savefig(output_dir + "/rf_frequency.png")
        np.save(output_dir + "/rf_frequency.npy", np.array([np.arange(0, N_t_on, 1), rf.omega_rf[0, 0:N_t_on]]))
        plt.clf()

        
        plt.plot(turns[1:], beam_phase[1:])
        plt.ylabel("Beam Phase")
        plt.xlabel("turns")
        plt.savefig(output_dir + "/beam_phase.png")
        np.save(output_dir + "/beam_phase.npy", np.array([turns, beam_phase]))
        plt.clf()
                
        plt.plot(turns[1:], beam_phase_east[1:])
        plt.ylabel("Beam Phase of East")
        plt.xlabel("turns")
        plt.savefig(output_dir + "/beam_phase_east.png")
        np.save(output_dir + "/beam_phase_east.npy", np.array([turns, beam_phase_east]))
        plt.clf()
    
    print("RF off")

    plots_tof.dt_plot = dt_plt_off
    plots_east.dt_plot = dt_plt_off
    full_tracker.dt_print = dt_plt_off
    plots_tof.separatix = False
    plots_east.separatix = False

    for i in range(N_t_on+ 1, N_t):
        # Plot has to be done before tracking (at least for cases with separatrix)
        if (i % dt_plt_off) == 0:
            print("Outputting at time step %d..." % i)
            print("   Beam momentum %.6e eV" % beam.momentum)
            print("   Beam gamma %3.3f" % beam.gamma)
            print("   Beam beta %3.3f" % beam.beta)
            print("   Beam energy %.6e eV" % beam.energy)
            print("   Four-times r.m.s. bunch length %.4e s" % (4.*beam.sigma_dt))
            print("")

        # Track
        for m in map_:
            m.track()
    
    if monitor_time_distribution:
        slices_monitor.close()
    if monitor_losses:
        losses_monitor.close()


if __name__ == '__main__':

        print("Simulating Phase jump with given parameters.")

        run(intensity_effects = False, phase_jump = True, feedback_loops = True, extraction_phase_glitch_correction= True,
            model = "C10,C20,C40,C80,C200,finemet, kickers,misc", delete_folder = True,

            # Times after 726 ms
            N_t_equilibrium=1000,
            N_t_before_jump = 208, N_t_j1= 236, N_t_m= 1469,
            N_t_j2 = 198, N_t_after=3000, N_t_off = 1, 

            dt_plt_on = 100, dt_plt_phase_jump = 50, dt_plt_off = 50,

            output_dir='output_files/santiy_check-settings',
            line_density_type_TOF = "binomial", line_density_exponent_TOF = 0.778, bunch_length_TOF = 47.43e-9,
            line_density_type_EAST = "binomial", line_density_exponent_EAST = 5, bunch_length_EAST = 84.173e-9,
            bunch_spacing_buckets = 4,

            start_time = 726, use_start_momentum=True,  # in ms
            accel_rate_cut = 52.4e3,

            slices_per_bucket = 2000,
            N_rf_sections = 1, N_macroparticles = int(1e4), N_200MHz_cavities = 0,
            n_turns_wake = 1, n_turns_FRONT_wake = 2,

            voltage = 200000, intensities = [355e10, 28e10],  # Adjust, [TOF_intensity, EAST_intensity]
            pj1_amp_to=2 * np.pi * (141.63 / 360), pj1_amp_back=2 * np.pi * ((141.63 + 24.53) / 360),
            pj2_amp_to=np.pi, pj2_amp_back=np.pi)
