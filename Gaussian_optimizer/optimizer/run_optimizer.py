from Gaussian_optimizer.data_processing.PS_bunch_evaluation import process_east_tof, calculate_bunch_parameters, calculate_front_tails 
import Gaussian_optimizer.condor_submission.submission_manager as sm
import time
import numpy as np
import os
import subprocess
import h5py
from functools import partial
import matplotlib.pyplot as plt
from skopt.plots import plot_gaussian_process
from skopt import Optimizer
from skopt.space.space import Real
from skopt.learning import GaussianProcessRegressor
import pickle
# Gaussian process with Matérn kernel as surrogate model
from skopt.learning.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)
np.int = np.int64 # Hack to fix depreciated numpy import in sklearn.
"""
Script to run a given optimizer.

Author: **Oleksandr Naumenko**

Consists of 3 parts:

1. Loss Function to evaluate the quality of simulation parameters from the simulation's output data
2. The function to run the simulation, taking as input the simulation parameters
3. The Optimizer itself. In this case, it's a Gaussian Process with Matern Kernel.

In this example, we look at the PS BIGTOFEAST cycle, with 2 bunchesL: EAST and TOF. The TOF bunch is extracted
during an intermediate flat top at 729 ms cycle time, after which the EAST bunch continues to accelerate.
The TOF bunch is rotated by phase jump before extraction, which also rotates the EAST bunch.
This rotation of EAST needs to be compensated after TOF extraction by a second bunch rotation.
The example studies the effects of these bunch rotations, with the parameter space scanned being:

1. the amplitude of the 1st jump towards the unstable phase
2. the amplitude of the 1st jump back from the unstable phase to the stable phase
3. the start time of the 1st jump
4. the duration of the 1st jump
5. the start time of the 2nd jump
6. the duration of the 2nd jump


Adjust the following global variables to define how the optimizer should be run.
For further adjustments, e.g. optimizing a different cylcle or timeframe, all 3 parts,
i.e.e the loss function, the function used to run the simulation and the
optimizer need to be adjusted.
"""

# Number of Macroparticles for the simulation
N_PARTICLES = int(1e6)
# Directory where the simulation is run. Contains the Condor logfiles
SIMDIR = "./simdir/"
# Directory for the output of the simulation, i.e. the bunch profiles, etc.
OUTDIR = os.path.join(os.path.realpath(SIMDIR), "output") #'/eos/user/o/onaumenk/Documents/Simulation_Results/6paramOptimizer_1000_equil_turns_test/'
# Email to notify about simulation progress
EMAIL = None # "oleksandr.naumenko@cern.ch"
# Iterations performed by the optimizer. Each iteration consists of 10 simulations,
# submitted in parallel
ITERS = 20
# If not None, will load an optimizer from the given file and continue the optimization
LOADPATH = None
# Maximal runtime alloted to your simulation, in seconds. Smaller values mean that
# HTCondor will prioritze these simulations more, but if the simulation takes
# over the maximal runtime, it will be aborted
RUNTIME = 3*60*60
# Path/Name of the mainfile defining the simulation
this_directory = os.path.dirname(os.path.realpath(__file__))
MAINFILE_NAME = os.path.join(this_directory,"PS_EastBigTof_optimizer")
# Path to the bashrc you wish to initialise in HTCondor (e.g. the one activating your desired conda environment)
BASHRC_PATH = "~/.bashrc"
# Points which the optimizer initially evluates to gain a basic understanding
# of the parameter space.
INIT_POINTS = [(152/360*2*np.pi, 24/360*2*np.pi, 250,222, 1357,191),
               (170/360*2*np.pi, 10/360*2*np.pi, 250,222, 1357,191),
               (180/360*2*np.pi, 0/360*2*np.pi, 250,222, 1357,191),
               (152/360*2*np.pi, 24/360*2*np.pi, 250,235, 1357,191),
               (152/360*2*np.pi, 24/360*2*np.pi, 250,205, 1356,191),
               (152/360*2*np.pi, 24/360*2*np.pi, 250,222, 1400,191),
               (152/360*2*np.pi, 24/360*2*np.pi, 250,222, 1357,240),
               (152/360*2*np.pi, 24/360*2*np.pi, 200,222, 1357,191),
               (152/360*2*np.pi, 24/360*2*np.pi, 50,222, 1357,191),
               (152/360*2*np.pi, 24/360*2*np.pi, 350,222, 1357,191)]
"""
1. Loss Function Evaluation from Simulation Output Data
"""



def calculate_loss_fixed_bunchlength(tof_bunch_length, tof_front_tail, east_bunch_oscillation, tof_losses, east_losses, weights = [10,1,1,2,0.5]):
    """
    Calculate the loss based on various input parameters and weights. Calculates loss with a linear, weighted sum, i.e.
    .. math::
        \mathcal L = \sum_{i=1}^{n} w_{i} x_{i} 

    Parameters:
    tof_bunch_length [float]: TOF bunch length
    tof_front_tail [float]: TOF front tail (calculated as the length of the interval containing the first 2.5% of macroparticles)
    east_bunch_oscillation [float]: East bunch oscillation amplitude after both phase jumps
    tof_losses [float]: Tof losses (as a fraction of total macroparticles in the Tof bunch)
    east_losses [float]: East losses (as a fraction of total macroparticles in the East bunch)
    weights [list, optional): Weights for each parameter. Defaults to [10, 1, 1, 2, 0.5].

    Returns:
    [float]: The calculated loss, normalized to reference values for each paramater
    """
    if (tof_bunch_length < 26e-9) :
        bl_loss = 26e-9-tof_bunch_length
    elif (tof_bunch_length > 30e-9):
        bl_loss = tof_bunch_length - 30e-9
    else:
        bl_loss = 0
        
    loss = weights[0]/1e-9*bl_loss + weights[1]/3.8e-9*tof_front_tail + weights[2]/28e-9*east_bunch_oscillation + weights[3]/0.07*tof_losses + weights[4]/0.07*east_losses
    return loss/np.sum(weights)

def calculate_merit(data_path, start_idx, synchrotron_period=1000,  # in turns
                    tof_end_time=2.52, east_end_time=9.42  # in ms
                    ):
    """
    Calculates the 4 figures of merit determined: TOF bunch length, east bunch oscillation, tof losses, east losses
    Uses the functions defined in data_processing.PS_bunch_evaluation.py

    Parameters:
        data_path [str]: The path to the simulation data. Specifies a folder, must end with "/"
        start_idx [int]: The turn of the simulation at which the evaluation begins, i.e. the number of equilibrium turns
        synchrotron_period [int]: The synchrotron period in turns
        tof_end_time [float]: Tof extraction time in ms after start of evaluation
        east_end_time [float]: East extraction time in ms after start of evaluation
    Returns:
        [list]: List of the 5 figures of merit 
            1. TOF bunch length
            2. TOF front tail
            3. EAST bunch oscillation amplitude
            4. TOF losses
            5. EAST losses

    """
    # data_path = path to folder of sim
    dt, TOF_profiles, tof_time, dt_east, EAST_profiles, east_time = \
        process_east_tof(data_path, start_idx, sim_start_time=726, tof_end_time=tof_end_time,
                         east_end_time=east_end_time, tof_bucket=1,
                         east_bucket=5, turn_duration=2.09e-3)

    com_tof_s, bl_tof_s = calculate_bunch_parameters(dt, TOF_profiles)
    a = bl_tof_s
    tof_particles = np.sum(TOF_profiles, axis=1)
    try:
        idx = (np.arange(0, len(tof_particles), 1)[tof_particles < 0.5])[0] - 1  # Index of last nonzero tof profile
    except:
        idx = -1
    tof_bunch_length = bl_tof_s[idx]

    front_tail = calculate_front_tails(dt, TOF_profiles[idx])

    com_east_s, bl_east_s = calculate_bunch_parameters(dt_east, EAST_profiles)
    minimum = np.min(bl_east_s[-synchrotron_period:])
    maximum = np.max(bl_east_s[-synchrotron_period:])
    east_bunch_oscillation = np.abs(maximum - minimum)

    file = data_path + "losses.h5"
    with h5py.File(file, "r") as f:
        east_losses = np.array(f["losses/east_losses"])[-1]
        tof_losses = np.array(f["losses/tof_losses"])[-1]
    return tof_bunch_length, front_tail, east_bunch_oscillation, tof_losses, east_losses



def evaluate_loss_fixed_bunchlength(data_path, start_idx=1000, synchrotron_period=1000,  # in turns
                                    tof_end_time=2.52, east_end_time=9.42  # in ms
                                    ):
    """
    Evaluates the loss, based on the loss function "calculate_loss_fixed_bunch_length".


    Parameters:
        data_path [str]: The path to the simulation data. Specifies a folder, must end with "/"
        start_idx [int]: The turn of the simulation at which the evaluation begins, i.e. the number of equilibrium turns
        synchrotron_period [int]: The synchrotron period in turns
        tof_end_time [float]: Tof extraction time in ms after start of evaluation
        east_end_time [float]: East extraction time in ms after start of evaluation

    Returns:
        [float]: The evaluated loss
    """
    try:
        loss = calculate_loss_fixed_bunchlength(
        *calculate_merit(data_path, start_idx, synchrotron_period=synchrotron_period,
                         tof_end_time=tof_end_time, east_end_time=east_end_time))
    except:
        loss = 100
    return loss

"""
2. Simulation Running Script
"""

def run_simulation(simulation_variable_values, simulation_variable_names = []):
    """
    Takes a list of simulation parameters and runs the simulation.

    Arguments:
        var_list [list of 1D-arrays]: List of arrays, with each array containing the 6 simulation parameters varied for the optimizer. 
                                Submits X parallel simulation to HTCondor, where X is the number of arrays in var_list

        simulation_variable_names [list of str]: Names of the simulation parameters

    Returns:
        [list of strings]: List of simulation directories
    """
    simulation_variable_values = np.array(simulation_variable_values)
    if len(simulation_variable_names) != simulation_variable_values.shape[1]:
        raise ValueError("The number of simulation variable names must be equal to the number of simulation variable values")
    parameter_dictionary = {}
    for i in range(simulation_variable_values.shape[1]):
        parameter_dictionary[simulation_variable_names[i]] = simulation_variable_values[:,i]

    output_directory = OUTDIR
    simdir, n_sims = sm.create_and_submit_scripts(global_sim_params = {"N_macroparticles" : N_PARTICLES},
                       one_by_one_sim_params = parameter_dictionary,
                       all_by_all_sim_params = None,
                       simulation_directory = SIMDIR, output_directory = output_directory,
                       mainfile_name = MAINFILE_NAME,
                       bashrc_path = "~/.bashrc",
                       cores=4, runtime = RUNTIME,
                       email_to_notify = EMAIL)
    time_waited = 0
    while not sm.simulations_finished(simdir, n_sims):
        print("Waited " +str(int(time_waited/60)) +" minutes, "+ str(time_waited % 60)+ " seconds")
        time.sleep(30)
        time_waited = time_waited + 30

        if (time_waited % (30*60)) == 0:
            subprocess.run(["condor_q"])

        if (time_waited % (6*60*60)) == 0:
            subprocess.run(["kinit", "-R"])
            subprocess.run(["aklog"])
    print("Finished")
    outputs = []
    for i in range(n_sims):
        outputs.append((os.path.join(output_directory, "{:d}/".format(i)), ))
    return outputs

"""
3. The Gaussian Process

"""

def gaussian_optimize(run_func, eval_func, evals_per_iteration, iterations,
                      param_bounds,
                      kernel=1 * Matern(length_scale=1, length_scale_bounds=(0.1, 10), nu=1.5),
                      noise_level=0.1,
                      initial_points = INIT_POINTS):
    """
    Automatically optimizes a hard to evaluate function via Gaussian processes, while fitting the GP Kernels hyperparameters.

    Args:
        run_func [function]: Function used to run the simulation based on the suggestion of the gaussian process. Must have the following signature
            f(x1,x2,x3...):
                ...
                return output_list
            output_list is a list of output-tuples, used to run the evaluation function. E.g. output_list = [(eval_input1_1, eval_input1_2), (eval_input2_1, eval_input2_1)]

        eval_func [function]: Evaluation function, taking a tuple from the output list from run_func, and using that to assess the loss function of the simulation.
            Must have the following signature
            f(output1,output2,...):
                ...
            return loss
            Where loss is a (0d) float number.

        evals_per_iteration [int]: Amount of evaluations carried out simultaneously

        iterations [int]: Number of iterations. The total number of function evaluations is therefore evals_per_iteration*iterations

        param_bounds [array of 2-tuples]: Bounds of the parameter space to analyze. Must contain n 2-tuples, where n is the amount of arguments eval_func takes.
                                          Can also be a list of n Dimension objects

        kernel [kernel object]: Kernel of the gaussian process

        noise_level: [float]: Amplitude of noise of the evaluation function.

        initial_points [list of arrays]: List of arrays containing simulation parameters used as initial parameters for the optimization
    Returns:
        res [Scipy OptimizeResult]: Result of the optimization
        opt [Scikit Optimizer]: The optimizer object
    """

    gpr = GaussianProcessRegressor(kernel=kernel, alpha=noise_level ** 2,
                                   normalize_y=False, noise=None,
                                   n_restarts_optimizer=10)

    opt = Optimizer(param_bounds, base_estimator=gpr, n_initial_points=evals_per_iteration,
                    acq_optimizer="sampling", acq_func="gp_hedge")

    if initial_points:
        f_vals = []
        outputs = run_func(initial_points)
        for out in outputs:
            f_vals.append(eval_func(*out))
        res = opt.tell(initial_points, f_vals)

    for i in range(iterations):
        next_x = opt.ask(evals_per_iteration)
        outputs = run_func(next_x)
        f_vals = []
        for out in outputs:
            f_vals.append(eval_func(*out))
        res = opt.tell(next_x, f_vals)
        print(opt.models[-1].kernel_)
        with open(os.path.join(OUTDIR, 'optimizer_result_{:d}.pkl'.format(i)), 'wb') as f:
            pickle.dump(res, f)
        with open(os.path.join(OUTDIR, 'optimizer_{:d}.pkl'.format(i)), 'wb') as f:
            pickle.dump(opt, f)
    return res, opt

def load_run_optimizer(opt_file, run_func, eval_func, evals_per_iteration, iterations):
    """
    Reloads the optimizer from a previous run and continues the optimization.
    
    Args:
        opt_file [string]: Path to the optimizer pickle file from a previous optimizer run
        run_func [function]: Function used to run the simulation based on the suggestion of the gaussian process. Must have the following signature
            f(x1,x2,x3...):
                ...
                return output_list
            output_list is a list of output-tuples, used to run the evaluation function. E.g. output_list = [(eval_input1_1, eval_input1_2), (eval_input2_1, eval_input2_1)]

        eval_func [function]: Evaluation function, taking a tuple from the output list from run_func, and using that to assess the loss function of the simulation.
            Must have the following signature
            f(output1,output2,...):
                ...
            return loss
            Where loss is a (0d) float number.

        evals_per_iteration [int]: Amount of evaluations carried out simultaneously

        iterations [int]: Number of iterations. The total number of function evaluations is therefore evals_per_iteration*iterations
    Returns:
        res [Scipy OptimizeResult]: Result of the optimization
        opt [Scikit Optimizer]: The optimizer object
    """
    with open(opt_file, "rb") as f:
        opt = pickle.load(f)
    x_vals_evaluated = opt.Xi
    f_vals_evaluated = opt.yi
    N_runs = len(f_vals_evaluated)
    previous_iter = int(N_runs/evals_per_iteration)
    for i in range(iterations):
        next_x = opt.ask(evals_per_iteration)
        outputs = run_func(next_x)
        f_vals = []
        for out in outputs:
            f_vals.append(eval_func(*out))
        res = opt.tell(next_x, f_vals)

        print(opt.models[-1].kernel_)
        with open(os.path.join(OUTDIR, 'optimizer_result_{:d}.pkl'.format(i + previous_iter)), 'wb') as f:
            pickle.dump(res, f)  
        with open(os.path.join(OUTDIR, 'optimizer_{:d}.pkl'.format(i + previous_iter)), 'wb') as f:
            pickle.dump(opt, f)    
    return res, opt


"""MAIN SCRIPT"""
if __name__ == '__main__':
    # Names of the simulation parameters (kwargs in the mainfile) to be varied/scanned by the optimizer
    simulation_variable_names = ["pj1_amp_to", "pj1_amp_back", "N_t_before_jump", "N_t_j1", "N_t_m", "N_t_j2"]
    print("Setting up the optimizer")
    print("Simulation parameters varied for optimization: {}".format(simulation_variable_names))
    # Bounds of the simulation parameters, defines the parameter space for the optimizer
    pj1_to_bounds = Real(140/360*2*np.pi,2*np.pi*180/360)
    pj1_back_bounds = Real(140/360*2*np.pi,2*np.pi*180/360)
    pj1_startturn = (50,450, )
    pj1_dur = (202, 242, )
    pj2_startturn = (1200,1500)
    pj2_dur = (141,300)
    param_bounds = [pj1_to_bounds, pj1_back_bounds, pj1_startturn, pj1_dur, pj2_startturn, pj2_dur]
    if len(param_bounds) != len(simulation_variable_names):
        raise ValueError("The number of simulation variables must be match the number of variable bounds provided")
    print("Bounds") 
    for i in range(len(param_bounds)):
        print({simulation_variable_names[i]: param_bounds[i]})
    
    # Kernel parameters
    print("Setting up Kernel")
    kernel_length_scale = np.array([0.35, 0.5, 200, 20, 150, 75])
    length_scale_bounds = (1e-6, 1e6)
    kernel=1 * Matern(length_scale=kernel_length_scale,
                      length_scale_bounds=(1e-6, 1e6), nu=1.5)
    print(kernel)

    # Optimizer definition
    loss_func = evaluate_loss_fixed_bunchlength
    print("Creating the Optimizer")
    if LOADPATH is None:
        res, opt = gaussian_optimize(partial(run_simulation, simulation_variable_names = simulation_variable_names), 
                                     loss_func, # loss function calculation 
                                     evals_per_iteration=10, 
                                     iterations = ITERS,
                                     param_bounds=param_bounds,
                                     kernel=kernel,
                                     noise_level=0.1)
    else:
        res, opt = load_run_optimizer(LOADPATH,
                                       partial(run_simulation, simulation_variable_names = simulation_variable_names),
                                       loss_func, 
                                       evals_per_iteration=10, 
                                       iterations = ITERS)

    with open(os.path.join(OUTDIR, 'optimizer_result_final.pkl'), 'wb') as f:
        pickle.dump(res, f)
    with open(os.path.join(OUTDIR, 'optimizer_final.pkl'), 'wb') as f:
        pickle.dump(opt, f)    


