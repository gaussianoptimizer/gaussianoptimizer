"""
Created on Thu December 07 16:00:00 2023

@author: Oleksandr Naumenko

Creates the shell and Condor submission scripts to run a simulation on the cluster.
The parameters in those scripts may be changed by the set_arguments function
"""

import os


# Path to the python file that reads a yaml file and runs the corresponding simulation
_YAML_INPUT_SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
_YAML_INPUT_SCRIPT_PATH = os.path.join(_YAML_INPUT_SCRIPT_PATH, "run_sim_with_yaml.py")


SIMDIR = "./sim_runs/" # Working Directory

cores = 12             # Cores for Simulation
runtime= 48*3600       # Runtime in Seconds
brc_path = "/afs/cern.ch/user/o/onaumenk/brc.txt"  #BashRC Path
MAINFILE = "mainfile"                       # Path to python main file where your python installation will find it

NOTIFY_EMAIL = "oleksandr.naumenko@cern.ch"

def set_arguments(simulation_directory, mainfile_name, bashrc_path, CPU_cores, sim_runtime, email_to_notify):
    global SIMDIR
    SIMDIR = simulation_directory
    global cores
    cores = CPU_cores
    global runtime
    runtime  = sim_runtime
    global brc_path
    brc_path = bashrc_path
    global MAINFILE
    MAINFILE = mainfile_name
    global NOTIFY_EMAIL
    NOTIFY_EMAIL = email_to_notify

def main():
    SHELL_SCRIPT = os.path.join(SIMDIR, "exec.sh")
    _SIMRUNINFO = os.path.join(SIMDIR, "sim_run_list.txt")  # Location of the text files listing all simulations to be ran
    _CONDOROUTPUT = os.path.join(SIMDIR, "condorlogs")  # Location for the HTcondor logs
    # Remove previous logs is they exist for clarity
    if os.path.exists(_CONDOROUTPUT):
        for filename in os.listdir(_CONDOROUTPUT):
            if os.path.isfile(os.path.join(_CONDOROUTPUT, filename)) and \
                    ("err" in filename or "out" in filename or "log" in filename):
                os.remove(os.path.join(_CONDOROUTPUT, filename))
    print(f"Creating shell script {SHELL_SCRIPT}")

    with open(SHELL_SCRIPT, 'w') as f:
        f.write(f"#!/bin/bash\n")
        f.write(f"#Make sure the right versions are used.\n")
        f.write("gcc --version\n")
        f.write(f"python --version\n")
        f.write(f'source ' + str(brc_path) + '\n')
        f.write(f"export OMP_NUM_THREADS={cores}\n")

        f.write(f"SIMPATH=$1 \n")
        #f.write(f"cd SIMPATH\n")
        f.write(f"# Run the python script\n")
        f.write(
            f"python {_YAML_INPUT_SCRIPT_PATH} --file {MAINFILE} --config $SIMPATH/input.yml \n")

    SUBMISSION_SCRIPT = os.path.join(SIMDIR, "htcondor.sub")
    print(f"Creating submission script {SUBMISSION_SCRIPT}")
    with open(SUBMISSION_SCRIPT, 'w') as f:
        f.write(f"SIM_RUN_LIST            = {_SIMRUNINFO}\n")
        f.write(f"executable              = {SHELL_SCRIPT}\n")
        f.write('Universe                = vanilla\n')
        f.write("arguments               = $(simdir)\n")
        f.write('getenv                  = True\n')
        if NOTIFY_EMAIL is not None:
            f.write(f'notify_user             = {NOTIFY_EMAIL}\n')
            f.write('notification            = Always\n')
        else:
            f.write('notification            = Never\n')
        f.write('should_transfer_files   = YES\n')
        # f.write(f"output                 = {OUTPUT_FOLDER}/noise.$(ClusterId).$(ProcId).out\n")
        # f.write(f"error                  = {OUTPUT_FOLDER}/noise.$(ClusterId).$(ProcId).err\n")
        # f.write(f"log                    = {OUTPUT_FOLDER}/noise.$(ClusterId).log\n")
        f.write(f'error                = {_CONDOROUTPUT}/err.$(process).txt\n')
        f.write(f'output               = {_CONDOROUTPUT}/out.$(process).txt\n')
        f.write(f"log                    = {_CONDOROUTPUT}/log.$(process).txt\n")
        f.write(f"RequestCpus            = {cores} \n")
        f.write('+AccountingGroup = "group_u_BE.ABP.NORMAL"\n')
        f.write(f"+MaxRuntime = {runtime} \n")  # hours * 60 minutes * 60 secs/ minute
        f.write('queue simdir from $(SIM_RUN_LIST)')
    os.makedirs(_CONDOROUTPUT, exist_ok=True)

if __name__ == '__main__':
    main()