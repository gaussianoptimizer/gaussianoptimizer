# -*- coding: utf-8 -*-
"""
Created on Fri July 14 11:00:00 2023

@author: Konstantinos Iliakis
"""

import yaml
import os
from itertools import product

# The following global variables can be modified for each submission.

# directory to store the .yml config files
configs_dir = os.path.abspath('./sim_runs/')
output_dir = os.path.join(configs_dir, "output")


# Name of yaml input file
yaml_input_file = 'input.yml'

# Name of file that will contain a list of all generated configs
sim_run_list = os.path.join(configs_dir, 'sim_run_list.txt')

# These parameters correspond to global parameters that will be the same for all input files
global_params = {'N_b': 1e9,
                 'N_t': 2000}

# The values of each parameter will be zipped element-wise (first with first, second with second, etc)
# and one input file will be created for each combination. All lists must have the same length.
one_by_one = {'p_i': [450e9, 460e9, 470e9],
              'p_f': [460e9, 470e9, 480e9]}

# The values of each parameter will be combined with all the values of the other parameters
# and one input file will be created for each combination. Lists may have different lengths.
all_by_all = None #{'p_i2': [410e9, 420e9],
             # 'p_f2': [430e9, 440e9]}

# If the name style is 'descriptive', the directory names will be the concatenation of the configuration parameters. 
# Be careful of the 255 character limit for filenames
# If the name style is 'sequential', an increasing number (i.e. 0, 1, ...) will be assigned to each new configuration.
name_style = 'sequential'

def set_arguments(simulation_directory, output_directory, global_parameters, one_by_one_parameters, all_by_all_parameters):
    global configs_dir
    configs_dir = os.path.abspath(simulation_directory)
    global sim_run_list
    sim_run_list = os.path.join(configs_dir, 'sim_run_list.txt')
    global global_params
    global_params = global_parameters
    global one_by_one
    one_by_one = one_by_one_parameters
    global all_by_all
    all_by_all = all_by_all_parameters
    global output_dir
    output_dir = output_directory


def generate_configs(keys, configs, start_i=0):
    all_configs = {}
    for config in configs:
        params = dict(zip(keys, config))
        if name_style == 'descriptive':
            config_key = '_'.join([f'{k}{v}' for k, v in params.items()])
        else:
            config_key = start_i + len(all_configs)
        params.update(global_params)
        params['output_dir'] = os.path.join(output_dir, str(config_key))
        all_configs[config_key] = params
    return all_configs

def main():
    os.makedirs(configs_dir, exist_ok=True)

    all_configs = {}

    # First the one_by_one configs are created
    if one_by_one is not None:
        keys, values = zip(*one_by_one.items())
        all_configs.update(generate_configs(keys, zip(*values)))

    # Then the all_by_all configs are created
    if all_by_all is not None:
        keys, values = zip(*all_by_all.items())
        all_configs.update(generate_configs(keys, product(*values), start_i=len(all_configs)))

    # Finally the configs are written to files
    print(
        f'Found {len(all_configs)} configs.\nCreating simulation run dirs in {configs_dir}')
    
    seq_num = 0
    list_file = open(sim_run_list, 'w')
    for key, params in all_configs.items():
        print(key, params)
        var_dir = os.path.join(configs_dir, str(key))
        os.makedirs(var_dir, exist_ok=True)
        with open(os.path.join(var_dir, yaml_input_file), 'w') as file:
            yaml.dump(params, file, sort_keys=False)
        list_file.writelines(var_dir + '\n')
        seq_num += 1

    list_file.close()
    print('Done generating config dirs.')
    print(f'All configs listed in {sim_run_list}.')
    # with open(sim_run_list, 'w') as outfile:
    #     outfile.writelines('\n'.join(list(all_configs.keys())))
    return len(all_configs)

if __name__ == '__main__':
    main()
