"""
Created on Thu December 07 16:00:00 2023

@author: Oleksandr Naumenko

Used to run a simulation from the command line by passing this script the yaml file of kwargs for the simulation in
the mainfile specified.

Takes 2 command line arguments:
    --config: Defines the path to the yaml file defining a dictionary that specifies simulation kwargs
    --file: Defines the name of the mainfile to be exectued with the kwargs given by the yaml file.

For YAML to correctly read numpy datatypes and other, non-standard objects, it must use the unsafe loader, which
can execute arbitrary code. This functionality is disabled by default, but can be enabled by setting
UNSAFE_LOAD in this file to True.
"""

import os
import argparse
import yaml
import importlib
import sys

# This will automatically be added to the PYTHONPATH if given
MAINFILE_PATH = None #"C:/Users/onaumenk/cernbox/Documents"
UNSAFE_LOAD = True

def main():
    if MAINFILE_PATH:
        import sys
        sys.path.insert(0, MAINFILE_PATH)

    parser = argparse.ArgumentParser(description='Run main file.')
    parser.add_argument('-c', '--config', type=str, help='Yaml input configuration file.')
    parser.add_argument('-f', '--file', type=str, help='Name of Mainfile')
    args = parser.parse_args()

    if args.config is not None:
        mainfile_path, _MAINFILE = os.path.split(args.file)
        mainfile_path = mainfile_path + "/"
        sys.path.insert(0, mainfile_path)
        _MAINFILE = importlib.import_module(_MAINFILE)
        # Load config file
        with open(args.config,'r') as inputfile:
            if UNSAFE_LOAD:
                params = yaml.load(inputfile, Loader=yaml.UnsafeLoader)
            else:
                params = yaml.load(inputfile, Loader=yaml.FullLoader)
            output_dir = params.get('output_dir')

    os.makedirs(output_dir, exist_ok=True)
    print(os.path.abspath(output_dir))

    _MAINFILE.run(**params)
    print("Output written to " + output_dir)
    print("Simulation ran successfully")


if __name__ == '__main__':
    main()
