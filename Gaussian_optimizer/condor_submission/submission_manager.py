"""
Created on Thu December 07 16:00:00 2023

@author: Oleksandr Naumenko
"""


"""
Manager for dynamically creating and submitting simulations to HTCondor
Automatically submits the simulation script in a given mainfile to HTCondor with given kwargs.

PACKAGE REQUIREMENTS:
- YAML
- Only for the example: numpy

FORMATTING SPECIFICATIONS:
- prepare_runs.py, run_sim_with_yaml.py, submission_creation.py and submission_manager.py must be in the same folder
- the python installation running this script should be able to find the mainfile you define
- the mainfile has a run(output_dir = output_directory, **other_kwargs) method used to run your desired simulation. 
- the mainfile run function has a kwarg named output_dir specifying the output directory
- The dictonaries defining the simulation parameters (e.g. GLOBAL_SIM_PARAMS, ALLBYALL_PARAMS, ONEBYONE_PARAMS) must
  have keys matching the name of the other kwargs in mainfile.run((output_dir = output_directory, **other_kwargs)
  
This script is designed to be run dynamically by another python program. 
To do so import this file and  use the user-facing method create_and_submit_scripts().

Alternatively, to run this script from the command line, specify the ALLCAPS values below and simply run
python3 submission_manager.py

When using this script dynamically, it returns the simulation directory and number of simulations submitted.
To monitor whether the submitted simulations are finished running, these outputs can be input into the function 
    simulations_finished(simulation_directory, config_number)
which returns True if all simulations have finished running.
"""

import sys
import os
import time
import subprocess
import itertools
import numpy as np

this_directory = os.path.dirname(os.path.realpath(__file__))
package_dir = os.path.realpath(os.path.join(this_directory, ".."))
sys.path.insert(0, this_directory)
try:
    import yaml
except:
    raise EnvironmentError("Please install PyYaml to use this script.")

import submission_creation
import prepare_runs
DEBUG = False

# These parameters correspond to global parameters that will be the same for all input files
GLOBAL_SIM_PARAMS = {'N_macroparticles': int(1e6), 'N_t_equilibrium': 60000}

# The values of each parameter will be zipped element-wise (first with first, second with second, etc)
# and one input file will be created for each combination. All lists must have the same length.
ONEBYONE_PARAMS = {'N_t_before_jump': [256, 207  ], 'N_t_j1': [204, 222], 'N_t_m' : [1409, 1434  ], 
                'N_t_j2' : [215, 193], 'pj1_amp_to' : [2*np.pi*(148.916)/360, 2*np.pi*(166.835)/360 ],
                'pj1_amp_back' : [2*np.pi*(148.916 - 12.36)/360, 2*np.pi*(166.835 + 5.276)/360], 
                "intensity_effects" : [True,True]}

# The values of each parameter will be combined with all the values of the other parameters
# and one input file will be created for each combination. Lists may have different lengths.
ALLBYALL_PARAMS = None

# Directory for the intermediate simulation files and condor logs
SIM_DIRECTORY = "./sim_runs/"

# ABSOLUTE directory for the output of the simulation (e.g. profile data, any plots saved, etc)
# Again: The run method of your mainfile must have a kwarg named output_dir defining where the output goes
OUT_DIRECTORY = os.path.join(os.path.join(os.path.realpath("./sim_runs/")), "output") #"/eos/user/o/onaumenk/Documents/Simulation_Results/test_scripting/"

# The name of your mainfile
# Again: The mainfile must be located somewhere the python installation that is running this file can find it
# This means that if the mainfile is not in the same folder as this file, you must specify its location in your
# bashrc (via adding the directory to the PYTHONPATH) that is then passed to HTCondor. Alternatively, you can specify
# the PYTHONPATH addition in your e.g. conda environment
MAINFILE_NAME = os.path.join(package_dir, "optimizer/","PS_EastBigTof_optimizer")

# Location of your bashrc
BASHRC = "~/.bashrc" # "/afs/cern.ch/user/o/onaumenk/.bashrc"

# Amount of cores requested from HTCondor for each simulation
CORES = 4

# Maximum runtime allotted to each simulation, in seconds
RUNTIME = 1*60*60

# Email to notify about errors or completion of the condor simulation. May be set to None to receive no notifications.
EMAIL = None #"oleksandr.naumenko@cern.ch"

KERBEROS_REAUTH = True # Reauthanticate with Kerberos on Lxplus every time a batch is submitted
def create_and_submit_scripts(global_sim_params = GLOBAL_SIM_PARAMS,
                   one_by_one_sim_params = ONEBYONE_PARAMS,
                   all_by_all_sim_params = ALLBYALL_PARAMS,
                   simulation_directory = SIM_DIRECTORY, output_directory = OUT_DIRECTORY,
                   mainfile_name = MAINFILE_NAME,
                   bashrc_path = BASHRC,
                   cores=CORES, runtime = RUNTIME,
                   email_to_notify = EMAIL):
    """


    :param global_sim_params [dict]: These parameters correspond to global parameters that will be the same for all input files
    :param one_by_one_sim_params [dict]: The values of each parameter will be zipped element-wise (first with first, second with second, etc)
                                            and one input file will be created for each combination. All lists must have the same length.
    :param all_by_all_sim_params [dict]: The values of each parameter will be combined with all the values of the other parameters
                                          and one input file will be created for each combination. Lists may have different lengths.
    :param simulation_directory [str]: Directory for the intermediate simulation files and condor logs
    :param output_directory [str]: ABSOLUTE directory for the output of the simulation (e.g. profile data, any plots saved, etc)
                                    Again: The run method of your mainfile must have a kwarg named output_dir defining where the output goes
    :param mainfile_name [str]: The name of your mainfile
                                Again: The mainfile must be located somewhere the python installation that is running this file can find it
    :param bashrc_path [str]: Location of your bashrc
    :param cores [int]: Amount of cores requested from HTCondor for each simulation
    :param runtime [int]: Maximum runtime allotted to each simulation, in seconds
    :param email_to_notify [str]: Email to notify about errors or completion of the condor simulation. May be set to None to receive no notifications
    :return: simulation_directory [str], number of simulations submitted [int]
    """
    print(f"Output Directory: \"{output_directory}\" \n" )
    n_configs = create_scripts(global_sim_params = global_sim_params,
                   one_by_one_sim_params = one_by_one_sim_params,
                   all_by_all_sim_params = all_by_all_sim_params,
                   simulation_directory = simulation_directory, output_directory = output_directory,
                   mainfile_name = mainfile_name,
                   bashrc_path = bashrc_path,
                   cores=cores, runtime = runtime,
                   email_to_notify = email_to_notify)
    time.sleep(1)
    submit_scripts(simulation_directory = simulation_directory, config_nr =  n_configs)
    return simulation_directory, n_configs


def simulations_finished(simulation_directory, config_number):
    """
    Checks if all simulations with yaml files in the given simulation directory are finished running.

    :param simulation_directory [str]: Simulation directory containing the yaml files fed into the simulations.
    :param config_number [int]: Total number of submitted simulations
    :return: True if all the simulations are finished, False otherwise
    """

    for i in range(config_number):
        pth = os.path.join(simulation_directory, "condorlogs/out.{:d}.txt".format(i))
        exist = os.path.isfile(pth)
        if not exist:
            return False
        """
        else:
            last_lines = reverse_readline(pth)
            found = False
            for last_line in itertools.islice(last_lines, 100):
                if last_line == "Simulation ran successfully":
                    found = True
            if found is False and throwError:
                raise RuntimeError("Simulation ran, but unsuccessfully. Aborting.")
            elif found is False:
                print("Simulation Number {:d} failed to run sucessfully.".format(i+1))
                return False
        """
    return True

def simulations_succesful(simulation_directory, config_number):
    """
    Checks if all simulations with yaml files in the given simulation directory are finished running.

    :param simulation_directory [str]: Simulation directory containing the yaml files fed into the simulations.
    :param config_number [int]: Total number of submitted simulations
    :param abort [bool]: If True, will throw an error if at least one of the simulations failed to run fully
    :return: 2-tuple of ints: Amount of finished and successful simulations
    """
    succesful_sims = 0
    finished_sims = 0
    for i in range(config_number):
        pth = os.path.join(simulation_directory, "condorlogs/out.{:d}.txt".format(i))
        exist = os.path.isfile(pth)
        if exist:
            last_lines = reverse_readline(pth)
            finished_sims =  finished_sims + 1
            success = False
            for last_line in itertools.islice(last_lines, 100):
                if last_line == "Simulation ran successfully":
                    succesful_sims = succesful_sims + 1
                    success = True
            if success is False:
                print("Simulation Number {:d} failed to run sucessfully.".format(i+1))
    return finished_sims, succesful_sims

def create_scripts(global_sim_params = {'N_b': 1e7, 'N_t': 2000},
                   one_by_one_sim_params = {'p_i': [450e9, 460e9, 470e9], 'p_f': [460e9, 470e9, 480e9]},
                   all_by_all_sim_params = None,
                   simulation_directory = "./sim_runs/", output_directory = "/afs/cern.ch/user/o/onaumenk/Public/Submission_Scripting/sim_runs/output",
                   mainfile_name ="mainfile",
                   bashrc_path = "/afs/cern.ch/user/o/onaumenk/.bashrc",
                   cores=4, runtime = 1*60*60,
                   email_to_notify = "oleksandr.naumenko@cern.ch"):

    prepare_runs.set_arguments(simulation_directory, output_directory,
                               global_sim_params, one_by_one_sim_params, all_by_all_sim_params)
    config_nr = prepare_runs.main()

    submission_creation.set_arguments(simulation_directory,mainfile_name,
                                      bashrc_path,
                                      cores, runtime, email_to_notify= email_to_notify)
    submission_creation.main()
    return config_nr



def submit_scripts(simulation_directory = "./sim_runs/", config_nr = 1, monitorRuns = False):
    script_path = os.path.join(simulation_directory, "htcondor.sub")
    if DEBUG:
        subprocess.run(["cat", script_path])
    else:
        subprocess.run(["kinit", "-R"])
        subprocess.run(["aklog"])
        subprocess.run(["condor_submit", script_path])
    if monitorRuns:
        time_waited = 0
        while not simulations_finished(simulation_directory, config_nr):
            time.sleep(30) # Sleep for 30 seconds
            time_waited = time_waited + 30
            print("Waited " +str(int(time_waited/60)) +" minutes, "+ str(time_waited % 60)+ " seconds")
        return True
    else:
        return True

def seek_last_line(file):
    with open(file, 'rb') as f:
        try:  # catch OSError in case of a one line file
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b'\n':
                f.seek(-2, os.SEEK_CUR)
        except OSError:
            f.seek(0)
        return f.readline().decode()
def reverse_readline(filename, buf_size=8192):
    """A generator that returns the lines of a file in reverse order"""
    with open(filename, 'rb') as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            # remove file's last "\n" if it exists, only for the first buffer
            if remaining_size == file_size and buffer[-1] == ord('\n'):
                buffer = buffer[:-1]
            remaining_size -= buf_size
            lines = buffer.split('\n'.encode())
            # append last chunk's segment to this chunk's last line
            if segment is not None:
                lines[-1] += segment
            segment = lines[0]
            lines = lines[1:]
            # yield lines in this chunk except the segment
            for line in reversed(lines):
                # only decode on a parsed line, to avoid utf-8 decode error
                yield line.decode()
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment.decode()
if __name__ == '__main__':
    create_and_submit_scripts()