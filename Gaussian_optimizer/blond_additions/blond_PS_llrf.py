import numpy as np

from blond.utils import bmath as bm
from Gaussian_optimizer.blond_additions.beam_feedback_abc import BeamFeedback
"""
Custom BlonD LLRF Classes

"""
class BeamFeedback_PS(BeamFeedback):
    """
    Beam Feedback subclass for the PS. Implements 2 loops with their respective gains. Each loop can be
    deactivated by setting the respective gain to 0.

    1.  Phase Loop using PL_gain, using the phase difference
        between beam and RF (actual synchronous phase). The transfer function is

        .. math::
            \\Delta \\omega_{rf}^{PL} = - g_{PL} \\Delta \\varphi_{out}

        with the transfered phase being calculated as

        .. math::
            \\Delta \\varphi_{out} = g_{diff} (\\Delta\\varphi_{PL} - \\Delta \\varphi_{prev}) + g_{int} \\ Delta \\varphi_{out,prev}


        where the phase difference :math: \\Delta\\varphi_{PL} is calculated as
        .. math::
            \\Delta\\varph_{PL} = \\varphi_{beam} - (\\varphi_{RF}+\\varphi_{programmed offset})


    2.   Radial loop using RL_gain: a radial loop to remove
        long-term frequency drifts:

        .. math::
            \\Delta \\omega_{rf}^{RL} =  g_{RL} \\Delta \\rho_{out} ,

        with

        .. math::

            \\Delta \\rho_{out} = (1-g_{internal}) \\Delta \\rho + g_{internal} \\Delta \\rho_{prev}

    """

    def __init__(self, Ring, RFStation, Profile,
                 PhaseNoise=None, time_offset=None, window_coefficient=0, delay=0, 
                 phase_target_offset  = 0, radial_reference=0,
                 PL_gain=0, RL_gain=0,
                 gd_pl=5.704, gi_pl=1 - 8.66e-5, g_rl=1 - 1.853e-1):
        """

        :param Ring: Ring Object
        :param RFStation: RF Station Object
        :param Profile: Profile Object
        :param PhaseNoise: array with length n_turns + 1
                Phase Noise injected into the machine. Will add the given phase at specified turn to the rf phase.
        :param sample_dE [int]: Determines which particles to sample for mean energy calculation.
                Every <sample_dE>. particle is sampled
        :param time_offset [float]: The beam phase is calculated in the dt interval [<time_offset>,<profile.cut_right>],
                i.e. from the time_offset value to the rightmost bucket
        :param window_coefficient [float]: Band-pass filter window coefficient for beam phase calculation
        :param delay [int]: Turn from which the loops start to act
        :param radial_reference [float]: Reference radial offset for the radial loop.

        :param PL_gain [float]: Phase loop gain, in rad/s
        :param RL_gain [float]: Radial Loop gain, in 1/m s
        :param gd_pl [float]: Hardware determined differential gain of the phase loop
        :param gi_pl [float]: Hardware determined integral gain of the phase loop
        :param g_rl [float]: Hardware determined radial loop gain parameter
        """

        super().__init__(Ring, RFStation, Profile,
                         PhaseNoise, time_offset, window_coefficient, delay, radial_reference)
        
        if not isinstance(phase_target_offset, (list, tuple, np.ndarray)):
                phase_target_offset = phase_target_offset * np.ones(len(RFStation.phi_s))
        self.phi_target_offset = phase_target_offset

        self.PL_gain = PL_gain
        self.RL_gain = RL_gain
        self.gi_pl = gi_pl
        self.gd_pl = gd_pl
        self.g_rl = g_rl

        self.prev_in_phase = 0
        self.prev_out_phase = 0
        self.prev_out_radial = 0

    def adjust_rf_frequency(self):
        """
        Calculates the frequency adjustment due to PS Phase and Radial Loops

        :return: Angular frequency correction due to loops domega_rf
        """

        dphi = self.phase_difference()
        drho = self.radial_difference()

        # Frequency correction from phase loop and radial loop
        dphi_out = self.gd_pl * (dphi - self.prev_in_phase) + self.gi_pl * self.prev_out_phase
        self.domega_dphi = - self.PL_gain * dphi_out
        self.prev_in_phase = dphi
        self.prev_out_phase = dphi_out

        drho_out = (1 - self.g_rl) * drho + self.g_rl * self.prev_out_radial
        self.domega_dR = self.RL_gain * drho_out
        self.prev_out_radial = drho_out

        return self.domega_dphi + self.domega_dR
    
    def phase_difference(self):
        '''
        *Phase difference between beam and RF phase of the main RF system.
        Optional: add RF phase noise through dphi directly.*
        '''
        self.phi_beam = self.beam_phase()
        # Correct for design stable phase
        counter = self.rf_station.counter[0]
        self.phi_target = self.rf_station.phi_s[counter]
        self.phi_target += self.phi_target_offset[counter]
        # Force target phase to be in the range [pi/2, 3/2 pi], same as beam phase
        # effectively, while phi_target <np.pi /2:
            # phi_target = phi_target + np.pi
        # and analogous for the other side
        if self.phi_target < np.pi/2:
            factor = np.ceil(np.abs((np.pi/2-self.phi_target)/np.pi))
            self.phi_target += np.pi * factor
        elif self.phi_target > 3/2*np.pi:
            factor = np.ceil(np.abs((self.phi_target- 3*np.pi/2)/np.pi))
            self.phi_target -= np.pi * factor

        self.dphi = self.phi_beam  - self.phi_target
        return self.dphi
    
    def beam_phase_east(self,time_offset):
        '''
        *Copy of the BlonD Beam Phase calculation function, with an additional time_offset parameter, which can
        be dynamically adjusted without changing the attributes of the feedback loop object
        '''

        # Main RF frequency at the present turn
        omega_rf = self.rf_station.omega_rf[0, self.rf_station.counter[0]]
        phi_rf = self.rf_station.phi_rf[0, self.rf_station.counter[0]]


        indexes = self.profile.bin_centers >= time_offset
        # Convolve with window function
        scoeff = np.trapz(np.exp(self.alpha * (self.profile.bin_centers[indexes] -
                                                time_offset)) *
                            np.sin(omega_rf * self.profile.bin_centers[indexes] +
                                    phi_rf) *
                            self.profile.n_macroparticles[indexes],
                            dx=self.profile.bin_size)
        ccoeff = np.trapz(np.exp(self.alpha * (self.profile.bin_centers[indexes] -
                                                time_offset)) *
                            np.cos(omega_rf * self.profile.bin_centers[indexes] +
                                    phi_rf) *
                            self.profile.n_macroparticles[indexes],
                            dx=self.profile.bin_size)
        coeff = scoeff / ccoeff

        # Project beam phase to (pi/2,3pi/2) range
        phi_beam = np.arctan(coeff) + np.pi
        return phi_beam