from blond.beam.profile import Profile, CutOptions, FitOptions, FilterOptions, OtherSlicesOptions
import numpy as np
""" 
Profile measuring according to an RF clock, i.e. shifting
with RF frequency shifts induced by e.g. feedback loops.

Author: **Oleksandr Naumenko**
"""

class ComovingProfile(Profile):
    def __init__(self, Beam,  RFStation, CutOptions=CutOptions(),
                 FitOptions=FitOptions(),
                 FilterOptions=FilterOptions(),
                 OtherSlicesOptions=OtherSlicesOptions(direct_slicing=False)):
        super().__init__(Beam, CutOptions = CutOptions, FitOptions = FitOptions,
                       FilterOptions=FilterOptions, OtherSlicesOptions = OtherSlicesOptions)

        self.rf = RFStation

    def track(self):
        counter = self.rf.counter[0]
        self.diff = 2*np.pi*self.rf.harmonic[0,counter]*(1/self.rf.omega_rf[0,counter]-1/self.rf.omega_rf_d[0,counter])
        cut_left = self.cut_left + (self.diff)
        cut_right = self.cut_right+  (self.diff)
        self.cut_options = CutOptions(cut_left, cut_right, self.cut_options.n_slices,
                                      n_sigma=self.cut_options.n_sigma, cuts_unit=self.cut_options.cuts_unit,
                                      RFSectionParameters=self.cut_options.RFParams)
        self.cut_options.set_cuts()
        self.set_slices_parameters()
        for op in self.operations:
            op()