
from builtins import object
import h5py as hp
import numpy as np
from blond.utils import bmath as bm
"""
Custom BlonD simulation monitors
"""
class MultiBunchMonitor(object):

    ''' Class able to save multi-bunch profile, i.e. the histogram derived from
        the slicing.

        The structure of the h5py file is as follows:

        default
        |
        |- profile (Beam profile)
        |- bin_centers (dt-coordinates corresponding to Beam profile)
        |- bin_centers_phase (phase coordinates corresponding to Beam profile)
        |- turns (turns, with turn[i] being the turn profile[i], etc. is recorded at)
        |- fwhm_bunch_position (Average bunch position calculated via FWHM)
        |- fwhm_bunch_length (Average bunch length calculated via FWHM)
        |---------
    '''

    def __init__(self, filename, n_turns, profile, rf, Nbunches, buffer_size=50):

        self.h5file = hp.File(filename + '.h5', 'w')
        self.filename = filename
        self.n_turns = n_turns
        self.i_turn = 0
        self.profile = profile
        self.rf = rf
        self.beam = self.profile.Beam
        self.h5file.create_group('default')
        self.h5group = self.h5file['default']
        self.Nbunches = Nbunches
        self.buffer_size = buffer_size
        self.last_save = 0
        self.n_macroparticles = self.beam.n_total_macroparticles

        self.create_data('profile', self.h5file['default'],
                         (self.n_turns, self.profile.n_slices), dtype='int32')

        self.b_profile = np.zeros(
            (self.buffer_size, self.profile.n_slices), dtype='int32')

        self.create_data('bin_centers', self.h5file['default'],
                         (len(self.profile.bin_centers),), dtype='float64')

        self.h5group['bin_centers'][:] = self.profile.bin_centers

        self.create_data('bin_centers_phase', self.h5file['default'],
                         (self.n_turns, self.profile.n_slices), dtype='int32')

        self.b_bin_centers_phase = np.zeros(
            (self.buffer_size, self.profile.n_slices), dtype='int32')


        self.create_data(
            'turns', self.h5file['default'], (self.n_turns, ), dtype='int32')
        self.b_turns = np.zeros(self.buffer_size, dtype='int32')

        self.create_data('losses', self.h5file['default'],
                         (self.n_turns, ), dtype='int')

        self.b_losses = np.zeros(
            (self.buffer_size, ), dtype='int32')

        self.create_data('fwhm_bunch_position', self.h5file['default'],
                         (self.n_turns, self.Nbunches), dtype='float64')
        self.b_fwhm_bunch_position = np.zeros(
            (self.buffer_size, self.Nbunches), dtype=float)

        self.create_data('fwhm_bunch_length', self.h5file['default'],
                         (self.n_turns, self.Nbunches), dtype='float64')
        self.b_fwhm_bunch_length = np.zeros(
            (self.buffer_size, self.Nbunches), dtype=float)

        if self.Nbunches == 1:
            # All these can be calculated only when single bunch
            self.create_data(
                'mean_dE', self.h5file['default'], (
                    self.n_turns, self.Nbunches),
                dtype='float64')
            self.create_data(
                'dE_norm', self.h5file['default'], (
                    self.n_turns, self.Nbunches),
                dtype='float64')

            self.create_data(
                'mean_dt', self.h5file['default'], (
                    self.n_turns, self.Nbunches),
                dtype='float64')

            self.create_data(
                'dt_norm', self.h5file['default'], (
                    self.n_turns, self.Nbunches),
                dtype='float64')

            self.create_data(
                'std_dE', self.h5file['default'], (self.n_turns, self.Nbunches),
                dtype='float64')

            self.create_data(
                'std_dt', self.h5file['default'], (self.n_turns, self.Nbunches),
                dtype='float64')

            self.b_mean_dE = np.zeros(
                (self.buffer_size, self.Nbunches), dtype=float)
            self.b_mean_dt = np.zeros(
                (self.buffer_size, self.Nbunches), dtype=float)

            self.b_dE_norm = np.zeros(
                (self.buffer_size, self.Nbunches), dtype=float)
            self.b_dt_norm = np.zeros(
                (self.buffer_size, self.Nbunches), dtype=float)

            self.b_std_dE = np.zeros(
                (self.buffer_size, self.Nbunches), dtype=float)
            self.b_std_dt = np.zeros(
                (self.buffer_size, self.Nbunches), dtype=float)


    def __del__(self):
        if self.i_turn > self.last_save:
            self.write_data()
        self.h5file.close()
        #print("closed")

    def write_buffer(self, turn):

        # Nppb = int(self.profile.Beam.n_macroparticles // self.Nbunches)
        # mean_dE = np.zeros(self.Nbunches, dtype=float)
        # mean_dt = np.zeros(self.Nbunches, dtype=float)
        # std_dE = np.zeros(self.Nbunches, dtype=float)
        # std_dt = np.zeros(self.Nbunches, dtype=float)
        # for i in range(self.Nbunches):
        #     mean_dE[i] = np.mean(self.profile.Beam.dE[i*Nppb:(i+1)*Nppb])
        #     mean_dt[i] = np.mean(self.profile.Beam.dt[i*Nppb:(i+1)*Nppb])
        #     std_dE[i] = np.std(self.profile.Beam.dE[i*Nppb:(i+1)*Nppb])
        #     std_dt[i] = np.std(self.profile.Beam.dt[i*Nppb:(i+1)*Nppb])

        idx = self.i_turn % self.buffer_size

        self.b_turns[idx] = turn
        self.b_profile[idx] = self.profile.n_macroparticles.astype(np.int32)
        self.b_bin_centers_phase[idx] = self.profile.bin_centers * self.rf.omega_rf[0,turn] + self.rf.phi_rf[0,turn]
       # self.b_losses[idx] = self.beam.losses
        self.b_fwhm_bunch_position[idx] = self.profile.bunchPosition
        self.b_fwhm_bunch_length[idx] = self.profile.bunchLength

        if self.Nbunches == 1:
            self.b_mean_dE[idx] = self.beam.mean_dE
            self.b_mean_dt[idx] = self.beam.mean_dt
            self.b_std_dE[idx] = self.beam.sigma_dE
            self.b_std_dt[idx] = self.beam.sigma_dt
            self.b_dE_norm[idx] = self.rf.voltage[0, turn]

            if turn == 0:
                self.b_dt_norm[idx] = self.rf.t_rev[0] * self.rf.eta_0[0] * \
                    self.rf.voltage[0, 0] / \
                    (self.rf.beta[0]**2 * self.rf.energy[0])
            else:
                self.b_dt_norm[idx] = self.rf.t_rev[turn] * self.rf.eta_0[turn] * \
                    self.rf.voltage[0, turn-1] / \
                    (self.rf.beta[turn]**2 * self.rf.energy[turn])

    def write_data(self):
        i1_h5 = self.last_save
        i2_h5 = self.i_turn
        i1_b = 0
        i2_b = self.i_turn - self.last_save
        # print("i1_h5, i2_h5:{}-{}".format(i1_h5, i2_h5))

        self.last_save = self.i_turn
        self.h5file = hp.File(self.filename + '.h5', 'r+')
        try:
            self.h5group['turns'][i1_h5:i2_h5] = self.b_turns[i1_b:i2_b]
            self.h5group['profile'][i1_h5:i2_h5] = self.b_profile[i1_b:i2_b]
            self.h5group['losses'][i1_h5:i2_h5] = self.b_losses[i1_b:i2_b]
            self.h5group['fwhm_bunch_position'][i1_h5:i2_h5] = self.b_fwhm_bunch_position[i1_b:i2_b]
            self.h5group['fwhm_bunch_length'][i1_h5:i2_h5] = self.b_fwhm_bunch_length[i1_b:i2_b]

            if self.Nbunches == 1:
                self.h5group['mean_dE'][i1_h5:i2_h5] = self.b_mean_dE[i1_b:i2_b]
                self.h5group['dE_norm'][i1_h5:i2_h5] = self.b_dE_norm[i1_b:i2_b]
                self.h5group['dt_norm'][i1_h5:i2_h5] = self.b_dt_norm[i1_b:i2_b]
                self.h5group['mean_dt'][i1_h5:i2_h5] = self.b_mean_dt[i1_b:i2_b]
                self.h5group['std_dE'][i1_h5:i2_h5] = self.b_std_dE[i1_b:i2_b]
                self.h5group['std_dt'][i1_h5:i2_h5] = self.b_std_dt[i1_b:i2_b]
        finally:
            self.h5file.close()

    def track(self, turn = None):
        if turn is None:
            turn = self.rf.counter[0]
        self.write_buffer(turn)
        self.i_turn += 1

        if (self.i_turn > 0) and (self.i_turn % self.buffer_size == 0):
            self.write_data()

    def create_data(self, name, h5group, dims, dtype):

        h5group.create_dataset(name, dims, compression='gzip',
                               compression_opts=4, dtype=dtype, shuffle=True)

    def close(self):
        if self.i_turn > self.last_save:
            self.write_data()
        self.h5file.close()


class LossesMonitor(object):

    ''' Class to calculate the losses for a parasitic TOF cycle with an EAST bunch and store the corresponding statistics in an h5 file

        The structure of the h5py file is as follows:
        
        losses
        |
        |- tof_losses (Losses of the TOF bunch, relative to the initial number of particles in the TOF bunch)
        |- east_losses (Losses of the EAST bunch, relative to the initial number of particles in the EAST bunch)
        |- total_losses (Total losses, relative to the initial number of particles)
        |- turns (turns, with turn[i] being the turn tof_losses[i], etc. is recorded at)
        |------------------
    '''

    def __init__(self, filename, n_turns, Ring, RF, Beam, cut_midpoint, bunch_macroparticles, N_t_extraction,
                 buffer_size=10000, beam_feedback = None, N_t_equilibrium = 0, N_t_accel_losses = None, delta_E_accel = 0):

        self.h5file = hp.File(filename + '.h5', 'a')
        self.filename = filename
        self.n_turns = n_turns - 1
        self.i_turn = 0
        self.beam = Beam
        self.ring = Ring
        self.rf = RF
        self.h5file.require_group('losses')
        self.h5group = self.h5file['losses']
        self.buffer_size = buffer_size
        self.last_save = 0
        self.n_macroparticles = self.beam.n_total_macroparticles
        self.tof_particles = int(bunch_macroparticles[0])
        self.east_particles = int(bunch_macroparticles[1])
        self.cut_midpoint = cut_midpoint
        self.N_t_extraction = N_t_extraction
        self.beam_feedback = beam_feedback
        self.N_t_equilibrium = N_t_equilibrium
        self.N_t_accel_losses = N_t_accel_losses
        self.delta_E_accel = delta_E_accel

        self.create_data('tof_losses', self.h5file['losses'],
                         (self.n_turns, ), dtype='float64')

        self.b_tof_losses = np.zeros(
            (self.buffer_size), dtype='float64')


        self.create_data('east_losses', self.h5file['losses'],
                         (self.n_turns, ), dtype='float64')

        self.b_east_losses = np.zeros(
            (self.buffer_size, ), dtype='float64')


        self.create_data(
            'turns', self.h5file['losses'], (self.n_turns, ), dtype='int32')
        self.b_turns = np.zeros(self.buffer_size, dtype='float64')

        self.create_data('total_losses', self.h5file['losses'],
                         (self.n_turns, ), dtype='float64')

        self.b_total_losses= np.zeros(
            (self.buffer_size, ), dtype='float64')




    def __del__(self):
        if self.i_turn > self.last_save:
            self.write_data()
        self.h5file.close()
        # print("closed")


    def losses_in_interval(self, left_edge = None, right_edge = None):
        """Cut only in a specified dt-interval, allowing to isolate specific bunches (e.g. EAST and TOF)
        :return [float]: Number of lost particles
        """
        if left_edge is None:
            left_edge = np.min(self.beam.dt)*1.1
        if right_edge is None:
            right_edge = np.max(self.beam.dt)*1.1
        cut_ids =self.beam.id[(self.beam.dt > left_edge) & (self.beam.dt < right_edge)]
        losses = cut_ids.shape[0] - bm.count_nonzero(cut_ids)
        return losses

    def write_buffer(self, turn):


        idx = self.i_turn % self.buffer_size

        self.b_turns[idx] = turn
        if turn == (self.N_t_extraction):
            self.final_tof_losses = self.losses_in_interval(right_edge=self.cut_midpoint)/self.tof_particles
        if turn >= (self.N_t_extraction):
            self.b_tof_losses[idx] = self.final_tof_losses
            self.b_east_losses[idx] = self.beam.n_macroparticles_lost / self.east_particles
        else:
            self.b_tof_losses[idx] = self.losses_in_interval(right_edge=self.cut_midpoint)/self.tof_particles
            self.b_east_losses[idx] = self.losses_in_interval(left_edge=self.cut_midpoint) / self.east_particles

        self.b_total_losses[idx] = (self.b_tof_losses[idx]*self.tof_particles + self.b_east_losses[idx]*self.east_particles)\
                                    /(self.n_macroparticles)


    def write_data(self):
        i1_h5 = self.last_save
        i2_h5 = self.i_turn
        i1_b = 0
        i2_b = self.i_turn - self.last_save
        # print("i1_h5, i2_h5:{}-{}".format(i1_h5, i2_h5))

        self.last_save = self.i_turn
        self.h5file = hp.File(self.filename + '.h5', 'r+')
        try:
            self.h5group['turns'][i1_h5:i2_h5] = self.b_turns[i1_b:i2_b]
            self.h5group['tof_losses'][i1_h5:i2_h5] = self.b_tof_losses[i1_b:i2_b]
            self.h5group['east_losses'][i1_h5:i2_h5] = self.b_east_losses[i1_b:i2_b]
            self.h5group['total_losses'][i1_h5:i2_h5] = self.b_total_losses[i1_b:i2_b]

        finally:
            self.h5file.close()

    def track(self, turn = None):
        """ Calculates the loss at each turn, expect for turns where a phase jump occurs

        """
        if turn is None:
            turn = self.rf.counter[0]
        # Phase jump if phase offset is too high
        if self.beam_feedback is not None and np.abs(self.beam_feedback.phi_target_offset[turn]) >= np.pi/2:
            pass
        # Phase jump if RF phase shifts too far off synchronous
        elif self.beam_feedback is None and np.abs(self.rf.phi_rf[0,turn]) >= np.pi/2:
            pass
        # Stops tracking losses around extraction due to the possibility that the RF frequency will shift too strongly.
        elif turn <= self.N_t_equilibrium or (turn > self.N_t_extraction and turn <= self.N_t_extraction + 100):
            pass
        elif self.N_t_accel_losses is not None and (turn >= self.N_t_accel_losses):
            self.beam.losses_separatrix_manual_acceleration(self.ring, self.rf, delta_E = self.delta_E_accel)
        else:
            self.beam.losses_separatrix_manual(self.ring,self.rf)
        self.write_buffer(turn)
        self.i_turn += 1

        if (self.i_turn > 0) and (self.i_turn % self.buffer_size == 0):
            self.write_data()

    def create_data(self, name, h5group, dims, dtype):

        h5group.create_dataset(name, dims, compression='gzip',
                               compression_opts=4, dtype=dtype, shuffle=True)

    def close(self):
        if self.i_turn > self.last_save:
            self.write_data()
        self.h5file.close()