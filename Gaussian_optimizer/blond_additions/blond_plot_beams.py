
# Copyright 2016 CERN. This software is distributed under the
# terms of the GNU General Public Licence version 3 (GPL Version 3), 
# copied verbatim in the file LICENCE.md.
# In applying this licence, CERN does not waive the privileges and immunities 
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
# Project website: http://blond.web.cern.ch/

'''
**Module to plot different bunch features**

:Authors: **Helga Timko**, **Danilo Quartullo**
'''

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from Gaussian_optimizer.blond_additions.blond_separatrix import custom_separatrix
from matplotlib.layout_engine import PlaceHolderLayoutEngine



def plot_long_phase_space(Ring, RFStation, Beam, xmin,
                          xmax, ymin, ymax, xunit = 's', sampling = 1, 
                          separatrix_plot = False, histograms_plot = True, 
                          dirname = 'fig', show_plot = False, alpha = 1, color = 'b',
                          delta_E = None):
    """
    Plot of longitudinal phase space. Optional use of histograms and separatrix.
    Choice of units: xunit = s, rad.
    For large amount of data, use "sampling" to plot a fraction of the data.
    """

    # Conversion from particle arrival time to RF phase
    if xunit == 'rad':
        omega_RF = RFStation.omega_rf[0,RFStation.counter[0]]
        phi_RF = RFStation.phi_rf[0,RFStation.counter[0]]

    # Definitions for placing the axes
    left, width = 0.115, 0.63
    bottom, height = 0.115, 0.63
    bottom_h = left_h = left+width+0.03
    
    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]

    # Prepare plot
    fig = plt.figure(1)
    fig.set_layout_engine(PlaceHolderLayoutEngine(False,False))
    fig.set_size_inches(8,8)
    axScatter = plt.axes(rect_scatter)
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)
    
    # Main plot: longitudinal distribution
    indlost = np.where( Beam.id[::sampling] == 0 )[0] # particles lost
    indalive = np.where( Beam.id[::sampling] != 0 )[0] # particles transmitted
    if xunit == 's':
        axScatter.set_xlabel(r"$\Delta t$ [s]", fontsize = 20)
        axScatter.scatter(Beam.dt[indalive], Beam.dE[indalive],
                          s=1, edgecolor='none', alpha=alpha, color=color)
        if len(indlost) > 0:
            axScatter.scatter(Beam.dt[indlost], Beam.dE[indlost], color='0.5',
                              s=1, edgecolor='none')
    elif xunit == 'rad':
        axScatter.set_xlabel(r"$\varphi$ [rad]",fontsize = 20)
        axScatter.scatter(omega_RF*Beam.dt[indalive] + phi_RF, 
                          Beam.dE[indalive], s=1, edgecolor='none', 
                          alpha=alpha, color=color)
        if len(indlost) > 0:
            axScatter.scatter(omega_RF*Beam.dt[indlost] + phi_RF, 
                              Beam.dE[indlost], color='0.5', s=1, 
                              edgecolor='none')
    axScatter.set_ylabel(r"$\Delta$E [eV]", fontsize = 20)
    axScatter.yaxis.labelpad = 1     
    xmin = xmin
    xmax = xmax
    axScatter.set_xlim(xmin, xmax)
    axScatter.set_ylim(ymin, ymax)
    text = axScatter.yaxis.get_offset_text()
    text.set_size(16)
    text = axScatter.xaxis.get_offset_text()
    text.set_size(16)
    
    axScatter.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.figtext(0.95,0.95,'%d turns' %RFStation.counter[0], 
                fontsize=24, ha='right', va='center')
            
    # Separatrix
    if separatrix_plot:
        x_sep = np.linspace(float(xmin), float(xmax), 1000)
        # Using the Custom Separatrix
        if xunit == 's':
            y_sep, shift = custom_separatrix(Ring, RFStation, x_sep, delta_E = None, shift = True)
            if delta_E is not None:
                y_sep_accel = custom_separatrix(Ring, RFStation, x_sep, delta_E=delta_E)

        elif xunit == 'rad':
            y_sep, shift = custom_separatrix(Ring, RFStation,  (x_sep - phi_RF)/omega_RF, delta_E = None, shift = True)
            if delta_E is not None:
                y_sep_accel = custom_separatrix(Ring, RFStation,  (x_sep - phi_RF)/omega_RF, delta_E=delta_E)
            
        #axScatter.plot(x_sep, y_sep, 'r', label = "Separatrix")
        #axScatter.plot(x_sep, - y_sep, 'r')
        # Shifted Separatrix
        axScatter.plot(x_sep, y_sep + shift, color = 'red', linestyle = "--", label = "Current Separatrix")
        axScatter.plot(x_sep, -y_sep + shift, color = 'red', linestyle = "--")
        # Possibly accelerating separatrix
        axScatter.plot(x_sep, y_sep_accel + shift, color = 'orange', linestyle = "--", label = "Accelerating Bucket after 1. flat top")
        axScatter.plot(x_sep, -y_sep_accel + shift, color = 'orange', linestyle = "--")
        
        dE_min = np.nanmax(y_sep_accel)*(0.1)*np.pi # Below this threshhold energy, the particles lie within the first approximately 10% of the bucket length
        # Area particles are considered captured in is marked in orange.
        axScatter.fill_between(x_sep, y_sep_accel*0.9 + shift, np.ones(len(x_sep))*shift, color = 'orange', alpha = 0.4, label = "Area of Captured Particles", zorder = -1, where = (y_sep_accel>= dE_min))
        axScatter.fill_between(x_sep, np.ones(len(x_sep))*shift, -y_sep_accel*0.9 + shift, color = 'orange', alpha = 0.4, zorder = -1, where = (y_sep_accel >= dE_min))
        axScatter.legend()
   
    # Phase and momentum histograms
    if histograms_plot:
        xbin = (xmax - xmin)/200.
        xh = np.arange(xmin, xmax + xbin, xbin)
        ybin = (ymax - ymin)/200.
        yh = np.arange(ymin, ymax + ybin, ybin)
      
        if xunit == 's':
            axHistx.hist(Beam.dt[::sampling], bins=xh, histtype='step', color=color)
        elif xunit == 'rad':
            axHistx.hist(omega_RF*Beam.dt[::sampling] + phi_RF, bins=xh, histtype='step', color=color)       
        axHisty.hist(Beam.dE[::sampling], bins=yh, histtype='step', 
                     orientation='horizontal', color=color)
        axHistx.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        axHisty.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        axHistx.axes.get_xaxis().set_visible(False)
        axHisty.axes.get_yaxis().set_visible(False)
        axHistx.set_xlim(xmin, xmax)
        axHisty.set_ylim(ymin, ymax)
        labels = axHisty.get_xticklabels()
        for label in labels:
            label.set_rotation(-90)


    # Output plot
    if show_plot:
        plt.show()
    else:
        fign = dirname +'/long_distr_'"%d"%RFStation.counter[0]+'.png'
        plt.savefig(fign)
    plt.clf()
