from __future__ import division, print_function
from builtins import range, object
from blond.trackers.utilities import time_modulo
import warnings
import numpy as np
def custom_separatrix(Ring, RFStation, dt, shift = False, delta_E = None):
    r""" Copy of the separatrix function from BlonD, with 2 additional features:
    1. Can calculate a shifted bucket due to an RF frequency shift, induced e.g. by the radial loop
    2. Takes a dynamically parameter defining the acceleration energy into account, instead of taking that value directly from the RF station
        Used to compute how many particles would be lost if the beam started accelerating.

    Parameters
    ----------
    Ring : class
        A Ring type class
    RFStation : class
        An RFStation type class
    dt : float array
        Time coordinates the separatrix is to be calculated for

    Returns
    -------
    if shift is False:
        float array
            Energy coordinates of the separatrix corresponding to dt

    if shift is True:
        (float array, float)
            Energy coordinates of the separatrix corresponding to dt, energy shift of the center of the bucket


    """

    warnings.filterwarnings("once")

    if Ring.n_sections > 1:
        warnings.warn("WARNING in separatrix(): the usage of several RF" +
                      " sections is not yet implemented!")

    # Import RF and ring parameters at this moment
    counter = RFStation.counter[0]
    voltage = Ring.Particle.charge*RFStation.voltage[:,counter]
    omega_rf = RFStation.omega_rf[:,counter]
    phi_rf = RFStation.phi_rf[:,counter]
    if delta_E is None:
        try:
            delta_E = RFStation.delta_E[counter]
        except:
            delta_E = RFStation.delta_E[-1]

    # Consider also the modulated phase when plotting the separatrix.
    if (RFStation.phi_modulation):
        dPhi = RFStation.phi_modulation[0]
        phi_rf = phi_rf - dPhi[:,counter]


    eta_0 = RFStation.eta_0[counter]
    beta_sq = RFStation.beta[counter]**2     
    energy = RFStation.energy[counter]
    T_0 = Ring.t_rev[counter]
    index = np.min( np.where(voltage > 0)[0] )
    T_rf_0 = 2*np.pi/omega_rf[index]


    # Projects time array into the range [t_RF, t_RF+T_RF] below and above
    # transition, where T_RF = 2*pi/omega_RF, t_RF = - phi_RF/omega_RF.
    # Note that the RF wave is shifted by Pi for eta < 0
    if eta_0 < 0:
        dt = time_modulo(dt, (phi_rf[0] - np.pi)/omega_rf[0],
                         2.*np.pi/omega_rf[0])
    elif eta_0 > 0:
        dt = time_modulo(dt, phi_rf[0]/omega_rf[0], 2.*np.pi/omega_rf[0])
    
    # Unstable fixed point in single-harmonic RF system
    if RFStation.n_rf == 1:
     
        dt_s = RFStation.phi_s[counter]/omega_rf[0]
        if eta_0 < 0:
            dt_RF = -(phi_rf[0] - np.pi)/omega_rf[0]
        else:
            dt_RF = -phi_rf[0]/omega_rf[0]
            
        dt_ufp = dt_RF + 0.5*T_rf_0 - dt_s
        if eta_0*delta_E < 0:
            dt_ufp += T_rf_0

    # Unstable fixed point in multi-harmonic RF system
    else:
        
        dt_ufp = np.linspace(-float(phi_rf[index]/omega_rf[index] - T_rf_0/1000), 
            float(T_rf_0 - phi_rf[index]/omega_rf[index] + T_rf_0/1000), 1002)

        if eta_0 < 0:
            dt_ufp += 0.5*T_rf_0 # Shift in RF phase below transition
        Vtot = np.zeros(len(dt_ufp))
        
        # Construct waveform
        for i in range(RFStation.n_rf):
            Vtot += voltage[i]*np.sin(omega_rf[i]*dt_ufp + phi_rf[i])
        Vtot -= delta_E
        
        # Find zero crossings
        zero_crossings = np.where(np.diff(np.sign(Vtot)))[0]
        
        # Interpolate UFP
        if eta_0 < 0:
            i = -1
            ind  = zero_crossings[i]
            while (Vtot[ind+1] -  Vtot[ind]) > 0:
                i -= 1
                ind = zero_crossings[i]
        else:
            i = 0
            ind = zero_crossings[i]
            while (Vtot[ind+1] -  Vtot[ind]) < 0:
                i += 1
                ind = zero_crossings[i]
        dt_ufp = dt_ufp[ind] + Vtot[ind]/(Vtot[ind] - Vtot[ind+1])* \
                 (dt_ufp[ind+1] - dt_ufp[ind])
        
    # Construct separatrix
    Vtot = np.zeros(len(dt))
    for i in range(RFStation.n_rf):
        Vtot += voltage[i]*(np.cos(omega_rf[i]*dt_ufp + phi_rf[i]) - 
                            np.cos(omega_rf[i]*dt + phi_rf[i]))/omega_rf[i]
                            
    separatrix_sq = 2*beta_sq*energy/(eta_0*T_0)*(Vtot + delta_E*(dt_ufp - dt))
    pos_ind = np.where(separatrix_sq >= 0)[0]
    separatrix_array = np.empty((len(separatrix_sq)))*np.nan
    separatrix_array[pos_ind] = np.sqrt(separatrix_sq[pos_ind])

    if shift is True:
        omega_rf_d = RFStation.omega_rf_d[:, counter]
        domega_rf = omega_rf - omega_rf_d
        shift = -domega_rf/omega_rf_d *1/eta_0 * beta_sq *energy
        if isinstance(shift, (list, tuple, np.ndarray)):
            shift = shift[0]
        return separatrix_array, shift
         
    return separatrix_array