from blond.beam.beam import Beam
from Gaussian_optimizer.blond_additions.blond_separatrix import custom_separatrix
import numpy as np
from blond.utils import bmath as bm
import matplotlib.pyplot as plt

""" 
Beam with custom definition of losses where no losses are 
calculated for periods when the phase jump occurs.

Author: **Oleksandr Naumenko**
"""
class CustomBeam(Beam):
    def __init__(self, Ring, n_macroparticles, intensity):
        super().__init__(Ring, n_macroparticles, intensity)

    def losses_separatrix_manual(self, Ring, RFStation, tolerance = 0.9):
        """Calculates losses based on a shifted separatrix, with the shift being created by RF frequency
        adjustments due to beam feedback loops. Lost particles' id's are set to 0, meaning they are greyed out
        in plots of the phase space.

        :param [object] Ring: BLonD Ring object describing the accelerator ring
        :param [object] RFStation: BLonD RFStation object, describing the RF system of the accelerator
        :param float tolerance: Fraction of the maximum energy above which particles are considered lost, defaults to 0.9
        """
        if RFStation.voltage[0,RFStation.counter[0]] == 0.0:
           return
        E_sep, shift = custom_separatrix(Ring, RFStation, self.dt, shift = True)
        max_dE = E_sep * tolerance + shift
        min_dE = -E_sep * tolerance + shift

        itemindex_above = bm.where(self.dE > max_dE)[0]

        itemindex_below = bm.where(self.dE < min_dE)[0]
        if itemindex_above.size != 0:
            self.id[itemindex_above] = 0
        if itemindex_below.size != 0:
            self.id[itemindex_below] = 0
        #  Below this threshhold energy, the particles lie within the first approximately 10% (for tolerance = 0.9) of the bucket length
        # This is calculated in 2nd order by approximating no acceleration and looking at the separatrix height at any point relative to the maximum height and how that relates to the phase
        # dE(phi) = sqrt(2 beta^2 E^2/eta q/T_rev V_rf/omega_rf * (1-cos(phi))) \approx sqrt(2 beta^2 E^2/eta q/T_rev V_rf/omega_rf * (phi^2/2))
        dE_min = np.max(E_sep)*(1-tolerance)*np.pi        

        itemindex_above = bm.where(E_sep < dE_min)[0]
        if itemindex_above.size != 0:
            self.id[itemindex_above] = 0

    def losses_separatrix_manual_acceleration(self, Ring, RFStation, tolerance = 0.9, delta_E = 0):
        """Calculates losses based on a shifted separatrix, with the shift being created by RF frequency
        adjustments due to beam feedback loops.
        Additionally takes into account "fake" acceleration, that is not present in simulation to consider acceleration losses after the timeframe of the simulation.
        Lost particles' id's are set to 0, meaning they are greyed out in plots of the phase space.

        :param [object] Ring: BLonD Ring object describing the accelerator ring
        :param [object] RFStation: BLonD RFStation object, describing the RF system of the accelerator
        :param [float] tolerance: Fraction of the maximum energy above which particles are considered lost, defaults to 0.9
        :param [float] delta_E: Energy shift due to acceleration, defaults to 0
        """
        if RFStation.voltage[0,RFStation.counter[0]] == 0.0:
           return
        
        # print("Treating Bucket as Accelerating for losses")
        E_sep, shift = custom_separatrix(Ring, RFStation, self.dt, shift = True, delta_E=delta_E)
        max_dE = E_sep * tolerance + shift
        min_dE = -E_sep * tolerance + shift

        itemindex_above = bm.where(self.dE > max_dE)[0]

        itemindex_below = bm.where(self.dE < min_dE)[0]
        if itemindex_above.size != 0:
            self.id[itemindex_above] = 0
        if itemindex_below.size != 0:
            self.id[itemindex_below] = 0

        #  Below this threshhold energy, the particles lie within the first approximately 10% (for tolerance = 0.9) of the bucket length
        # This is calculated in 2nd order by approximating no acceleration and looking at the separatrix height at any point relative to the maximum height and how that relates to the phase
        # dE(phi) = sqrt(2 beta^2 E^2/eta q/T_rev V_rf/omega_rf * (1-cos(phi))) \approx sqrt(2 beta^2 E^2/eta q/T_rev V_rf/omega_rf * (phi^2/2))
        dE_min = np.max(E_sep)*(1-tolerance)*np.pi        

        itemindex_above = bm.where(E_sep < dE_min)[0]
        if itemindex_above.size != 0:
            self.id[itemindex_above] = 0
        