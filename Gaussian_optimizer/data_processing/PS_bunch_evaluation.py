import numpy as np
import matplotlib as plt
from Gaussian_optimizer.data_processing.data_IO import index_of_value, import_profiles_simulation
from Gaussian_optimizer.data_processing.bunch_data_preprocessing import extract_profiles_at_bucket, FWHM_bunches, sigma_from_average_widths

"""
Functions to evaluate the quality of bunches (with default values selected for the PS BigTofEast cycle)

Author: **Oleksandr Naumenko**
"""

def process_east_tof(sim_path, start_idx, sim_start_time=726, tof_end_time=2.1, east_end_time=4, tof_bucket=1,
                     east_bucket=5, turn_duration=2.09e-3):
    """
        Process the simulation data for the EASTBIGTOF cycle, extracting Delta t coordinates of the profiles, the profiles and their corresponding turns.
        
        Args:
            sim_path [str]: Path to the simulation data
            start_idx [int]: Turn where to start the evaluation, i.e. the number of equilibrium turns before the actaul phase jump simulation starts.
            sim_start_time [float, optional]: Start time of the simulation in ms
            tof_end_time [float, optional]: Tof time of extraction in ms
            east_end_time [float, optional]: Where to end the east bunch evaluation, in ms
            tof_bucket [int, optional]: Bucket of the Tof bunch
            east_bucket [int, optional]: Bucket of the east bunch
            turn_duration [float, optional]: Duration of each revolution turn. Defaults to 2.09e-3.
        
        Returns:
            tuple: 6-Tuple  (dt, tof_profiles, tof_time, dt_east, east_profiles, and east_time)
                containing Delta t, profile and turn arrays for both Tof and East bunches
    """
    midpoints, profiles, turns_sim = import_profiles_simulation(sim_path + "profile_data.h5", start_idx=start_idx)
    turns_sim = turns_sim - start_idx
    dt, TOF_profiles = extract_profiles_at_bucket(midpoints, profiles, tof_bucket)
    dt_east, EAST_profiles = extract_profiles_at_bucket(midpoints, profiles, east_bucket)

    time = sim_start_time + turns_sim * turn_duration
    tof_end_idx = index_of_value(time - sim_start_time, tof_end_time)
    east_end_idx = index_of_value(time - sim_start_time, east_end_time)

    tof_profiles = TOF_profiles[:tof_end_idx]
    east_profiles = EAST_profiles[:east_end_idx]

    tof_time = time[:tof_end_idx]
    east_time = time[:east_end_idx]

    return dt, tof_profiles, tof_time, dt_east, east_profiles, east_time

def turn_of_time(time, PS_turn = 2.09e-6):
    return int(np.rint(time/PS_turn))


def calculate_com(dt, profiles, recenter=True):
    """
    Calculate the center of mass (COM) for the given profiles. The dt array is assumed to have length N, and the profiles array to have shape M x N,
     where M is the number of profiles, and N is the length of each individual profile
    
    Parameters:
    - dt [1D array, length N]: The time interval for the profiles.
    - profiles [N x M array]: The profiles from which to calculate the COM.
    - recenter [bool, optional]: Whether to recenter the COM around the initial value

    Returns:
    - com: The calculated center of mass.
    """
    if profiles.ndim > 1:
        com = np.sum(profiles * dt, axis=1) / np.sum(profiles, axis=1)
    else:
        com = np.sum(profiles * dt) / np.sum(profiles)
    if recenter:
        return com - com[0]
    else:
        return com




def calculate_bunch_parameters_depreciated(dt, profiles, recenter=True):
    """
        Calculate center of mass and bunch length for the bunch in given profiles. (Profiles must only contain 1 bunch)

        Args:
        dt [1D array, length N]: The time interval for the profiles.
        profiles [2D array, length M x N]: The profiles for which to calculate the parameters.
        recenter  [bool, optional]: Whether to recenter the profiles (default is True).
    
        Returns:
           com [float]: The center of mass calculated from the profiles.
           bunch_length [float]:  The calculated bunch length of the bunch in the profiles.
        """
    com = calculate_com(dt, profiles, recenter)
    fwhm = FWHM_bunches(profiles, dt[1] - dt[0])
    # fwhm = calculate_4sigma(profiles, dt[1]-dt[0], p0=(1e-7, 1e-7))

    bunch_length = fwhm / 2.355 * 4

    return com, bunch_length


def calculate_bunch_parameters(dt, profiles, recenter=True):
    """
        Calculate center of mass and bunch length for the bunch in given profiles. (Profiles must only contain 1 bunch)

        Args:
        dt [1D array, length N]: The time interval for the profiles.
        profiles [2D array, length M x N]: The profiles for which to calculate the parameters.
        recenter  [bool, optional]: Whether to recenter the profiles (default is True).
    
        Returns:
           com [float]: The center of mass calculated from the profiles.
           bunch_length [float]:  The calculated bunch length of the bunch in the profiles.
        """
    com = calculate_com(dt, profiles, recenter)
    fwhm = FWHM_bunches(profiles, dt[1] - dt[0])

    bunch_length_4sigma = 4*sigma_from_average_widths(profiles, dt[1]-dt[0], height_fractions = [10/i for i in range(2,9)])

    return com, bunch_length_4sigma

def calculate_front_tails(dt,profile, cumulative_tail_threshold = 0.025, noise_baseline = 0):    
    """
    Calculate the length of the front tail of a bunch in given profile. (Used for Tof bunch). 
    The front tail is defined as the Delta t interval in which the first 'cumulative_tail_threshold' fraction of particles of the bunch lie.

    Args:
        dt: [1D array, length N] The time interval for the profiles.
        profile: [1D array, length N] The profiles for which to calculate the parameters.
        cumulative_tail_threshold: [float, optional] The fraction of particles used to define the front tail
        noise_baseline: [positive float, optional] The baseline to the profile is shifted down by before calculating the front tail. Necessary for accurate calculation of the tail in measured data.
                                        If 0, will shift down by the minimum of the profile, which works well for simulated data, but not measured profiles.

    Returns:
        tail_length [float]: The length of the front tail
    """
    if noise_baseline > 0:
        profile = profile - noise_baseline
        profile = profile/np.sum(profile[profile > 0])
    else:
        profile = profile - np.min(profile)
        profile = profile/np.sum(profile)

    
    tail_sum = 0
    tail_idx = 0
    nonzero_indices = np.argwhere(profile)
    first_nonzero = nonzero_indices[0]
    while (tail_sum < cumulative_tail_threshold):
        tail_sum = tail_sum + profile[first_nonzero + tail_idx]
        tail_idx = tail_idx + 1
    
    tail_length = (dt[1]-dt[0])*tail_idx
    
    
    return tail_length