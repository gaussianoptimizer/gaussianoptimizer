from Gaussian_optimizer.data_processing.data_IO import index_of_value
import numpy as np
from scipy.signal import find_peaks

"""
Functions for preprocessing profile data from simulations to e.g.
extract bunches in individual buckets for further analysis

Author: **Oleksandr Naumenko**
"""

def extract_bunch_at_bucket(dt, profile, bucket_of_bunch, bucket_length=251.3e-9, renormalise=False):
    """
    Extracts profile data at given bucket.

    :param dt:
    :param profile: single profile
    :param bucket_of_bunch:
    :param bucket_length:
    :param renormalise:
    :return:
    """
    dt = dt - np.min(dt)
    start_idx = index_of_value(dt, bucket_length * (bucket_of_bunch - 1))
    end_idx = index_of_value(dt, bucket_length * bucket_of_bunch)

    half_bucket_length_idx = int((end_idx-start_idx)/2)
    profile_bunch = profile[start_idx:end_idx]

    # Centers the aquired bunch in the bucket. Bunch center is assumed to be the maximum, and then half the bucket width
    # to the left and right are aquired and returned
    bunch_profile_max_idx = start_idx + np.argmax(profile_bunch)
    profile_bunch = profile[bunch_profile_max_idx-half_bucket_length_idx: bunch_profile_max_idx+half_bucket_length_idx]
    dt_bunch = dt[bunch_profile_max_idx-half_bucket_length_idx: bunch_profile_max_idx+half_bucket_length_idx]
    dt_bunch = dt_bunch - np.min(dt_bunch)


    if renormalise:
        profile_bunch = profile_bunch - np.min(profile_bunch)
        profile_bunch = profile_bunch / np.sum(profile_bunch)

    return dt_bunch, profile_bunch


def extract_profiles_at_bucket(dt, profiles, bucket_of_bunch, bucket_length=251.3e-9):
    """
    Extracts profile data at given bucket over all frames.

    :param dt:
    :param profiles: waterfall
    :param bucket_of_bunch:
    :param bucket_length:
    :return:
    """
    start_idx = index_of_value(dt, bucket_length * (bucket_of_bunch - 1))
    end_idx = index_of_value(dt, bucket_length * bucket_of_bunch)
    dt = dt[start_idx:end_idx]
    dt = dt - np.min(dt)
    return dt, profiles[:, start_idx:end_idx]


def preprocess_bunch(dt, profile, expected_bunch_length=1e-9, max_iters=10, rel_height = 0.98):
    """
    Robust function to handle bunch preprocessing. Tries to remove a baseline > 0 close to the bunch in measurements.
    Done via scipy.signals.find_peaks to examine the bunch width and remove the baseline outside of that width.

    rel_height is the height at which the peak width is calculated, with 0 being the very tip and 1 being the full base.

    If no proper peak is found, does not do any extra preprocessing


    Returns time axis and processed profile
    """
    profile = profile - np.min(profile)
    profile = profile / np.sum(profile)
    # expected length of the bunch in terms of indices
    wlen = int(np.round(expected_bunch_length / (dt[1] - dt[0])))
    # expected prominence. if the bunch is aquired well, then it should be 70-80% of the profile maximum
    prom = np.max(profile) * 0.8
    peaks, param_dict = find_peaks(profile, height=prom, prominence=prom, wlen=wlen)
    i = 0
    j = 0
    while peaks.size == 0 and i < max_iters:
        # If no peak is found, increase the allowed bunch width first. This way, the lowest accurate bunch width is found
        wlen = wlen*1.1
        peaks, param_dict = find_peaks(profile, height=prom, prominence=prom, wlen=wlen, rel_height = rel_height)
        i = i + 1
        if (i == max_iters and j < max_iters):
            j = j +1
            prom = prom * 0.95
            i = 0
    # If no peak is found anyways, return data without preprocessing
    if (peaks.size == 0):
        print("No preprocessing done")
        return dt, profile
    left_idx = param_dict["left_bases"][0]
    right_idx = param_dict["right_bases"][0]

    profile_mod = np.copy(profile)
    # Otherwise remove baseline outside of peak
    profile_mod[:left_idx] = profile[:left_idx] - np.average(profile[:left_idx])
    profile_mod[right_idx:] = profile[right_idx:] - np.average(profile[right_idx:])
    profile_mod = profile_mod / np.sum(profile_mod)
    return dt, profile_mod

# Bunch Parameters

def determine_bunch_width(bunch, time_per_datapoint, height_fraction):
    """ 
    Determines the width of a given bunch at 1/height_fraction of its maximum value
    The FWHM for example is calculated with h=2, as it is the width at half of the bunch's height
    
    Args:
        bunch: [1D array, length N] The profile of the bunch
        time_per_datapoint: [float] The Delta t-spacing between each value of the bunch profile.
        height_fraction: [float] The fraction of the bunch height at which the width is calculated

    Returns:
        width [float]: The width of the bunch
    """
    y_threshold = (np.max(bunch) - np.min(bunch)).astype(np.float64)/height_fraction
    above_height = bunch > (y_threshold)
    width =  np.sum(above_height).astype(np.float64) * time_per_datapoint
    return width

def FWHM_bunches(bunches, time_per_datapoint):
    """Calculates the raw FWHM for each bunch given"""
    FWHMs = []
    for bunch in bunches:
        FWHM =  determine_bunch_width(bunch, time_per_datapoint, height_fraction= 2)
        FWHMs.append(FWHM)
    return np.array(FWHMs)


def sigma_from_average_widths(bunches, time_per_datapoint, height_fractions = [10/i for i in range(2,9)]):
    """
    Calculates the approximate sigma, i.e. standard deviation, of each bunch given, assuming it is roughly gaussian.
    This is done by averaging over the bunch's widths at different heights, determined via the height_fractions kwarg.
    Concretely, for a Gaussian bunch, the sigma can be determined from the width at the height 1/height_fraction via
    sigma = width/(2*sqrt(2*log(height_fraction)))
    This is calculated and averaged over for each height fraction given in the height_fractions list.

    Args:
        bunches [2D array, length MxN]: List of bunch profiles to be evaluated, i.e. a waterfall
        time_per_datapoint: [float] The Delta t-spacing between each value of each bunch profile.
        height_fractions [list, optional]: List of height fractions the widths are calculated for and then averaged over to obtain the standard deviation sigma.
            
            Defaults to [10/i for i in range(2,9)].

    Returns:
        sigmas [1D array, length M]: The standard deviation of each bunch
    """
    height_fractions = np.array(height_fractions)
    sigmas = []
    for bunch in bunches:
        bunch_width_list = np.array([determine_bunch_width(bunch,time_per_datapoint, h) for h in height_fractions])
        sigma_list = bunch_width_list/(2*np.sqrt(2*np.log(height_fractions)))
        sigma = np.average(sigma_list)
        sigmas.append(sigma)
    return np.array(sigmas)


def peak_bunches(bunches):
    """Calculates the peaks of each bunch given."""
    peaks = []
    for bunch in bunches:
        peaks.append(np.max(bunch))
    return peaks


def calculate_intensity(bunches, x=None):
    """Calculates the intensity of the bunch by integrating over the given bunch profile."""
    intensity = np.zeros(len(bunches))
    for i, bunch in enumerate(bunches):
        intensity[i] = np.trapz(bunch, x=x)
    return intensity