import numpy as np
import matplotlib.pyplot as plt
import h5py
from scipy.signal import butter, filtfilt
import os
"""
Functions for processing data produced by the simulations, to 
allow evaluation for the optimizer.

Author: **Oleksandr Naumenko**
"""


### PROFILE IMPORTS:


# Signal Filter for Measurement Data Cleaning
def butter_lowpass_filter_(data, cutoff, fs, order):
    normal_cutoff = cutoff /(0.5*fs)
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

# Import measured profiles
def import_profiles_measurement(file, frames, bins_per_frame=500, N_macroparticles=1, filter_data=False, filter_freq=0.05,
                     order=1, turns_per_frame = None, time_per_datapoint = None, start_turns = None):
    """
    Processes the profiles in given .npy file, outputting an array with each profile being an array element
    If N_macroparticles is passed, data is also renormalised to the amount of macroparticles.

    If only frames and bins_per_frame are given, returns only the waterfall of profiles.
    if time_per_datapoint is also given, also returns the dt axis for each profile. Return shape is (dt array, waterfall)
    If tunrs_per_frame is also given, returns the turns each profile corresponds to. Return shape is (dt array, waterfall, turns)
    """
    try:
        iter(frames)
    except TypeError:
        frames = [frames]
    n_frames = np.sum(frames)
    profiles = np.zeros((n_frames, bins_per_frame))
    data = np.load(file)
    for i in range(n_frames):
        p = data[int(bins_per_frame * i): int(bins_per_frame * (i + 1))]
        if filter_data:
            p = butter_lowpass_filter_(p, filter_freq, 1, 1)

        if N_macroparticles:
            p = p - np.min(p)
            p = p*N_macroparticles/np.sum(p)
        profiles[i] = p

    ret_list = []
    if time_per_datapoint:
        x = np.arange(0,len(profiles[0]))*time_per_datapoint
        ret_list.append(x)

    ret_list.append(profiles)

    if turns_per_frame:
        try:
            iter(turns_per_frame)
        except TypeError:
            turns_per_frame = [turns_per_frame]
        turns = np.zeros(np.sum(frames))
        start_frame = 0
        if start_turns:
            try:
                iter(start_turns)
            except TypeError:
                start_turns = [start_turns]
            start_turn = start_turns[0]
        else:
            start_turn = 0
        for i, tpf in enumerate(turns_per_frame):
            end_frame = start_frame+frames[i]
            turns[start_frame:end_frame] = start_turn + np.arange(0,frames[i])*tpf

            if start_turns and i < (len(start_turns)-1):
                start_turn = start_turns[i+1]
            else:
                start_turn = frames[i]*tpf + 1
            start_frame = end_frame


        ret_list.append(turns)
    return list(ret_list)

def import_profiles_simulation(file, start_idx = 0, end_idx = None, phase_coords = False):
    """
    Imports simulated profiles given the location of the corresponding h5 file.

    If phase_coords = True, then will output
        midpoints (phase coords), profiles, turns, midpoints (dt coords)
    else will output
        midpoints (dt coords), profiles, turns
    """
    with h5py.File(file, "r") as f:
        profiles = np.array(f["default/profile"][start_idx:])
        midpoints = np.array(f["default/bin_centers"])
        turns = np.array(f["default/turns"][start_idx:])
        if phase_coords:
            midpoints_phase = np.array(f["default/bin_centers_phase"][start_idx:])
    if end_idx:
        profiles = profiles[:end_idx]
        turns = turns[:end_idx]
        if phase_coords:
            midpoints_phase = midpoints_phase[:end_idx]
    if phase_coords:
        return midpoints_phase, profiles, turns, midpoints
    return midpoints, profiles, turns



def export_profiles(t_profile, profiles, turns, path,  filename):
    if path:
        filename = os.path.join(path,filename)
    os.makedirs(path, exist_ok=True)
    f = h5py.File(filename + ".hdf5", 'w')
    f.create_dataset("default/bin_centers", data = t_profile)
    f.create_dataset("default/profile", data = profiles)
    f.create_dataset("default/turns", data = turns)
    f.close()





### Utilities

def index_of_value(array, value):
    """
    Determines the index at which array is closest to value.
    """
    return np.argmin(np.abs(array - value))




