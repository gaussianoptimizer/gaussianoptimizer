# GaussianOptimizer

Package implementing a machine learning algorithm using Gaussian processes. This optimizer is specifically used to find the optimal settings for the first 2 bunch rotations by phase jump performed in the PS EASTBIGTOF cycle, after reaching the initial flat top. 

The [basic framework](OptimizerConcept.png) can be used for any cycle or timeframe however, if the simulation is adjusted to accurately reflect the cycle and timeframe studied. For further details on the concept of Gaussian processes, I recommend the following book:

-   C. E. Rasmussen and C. K. I. Williams, *Gaussian processes for machine learning*; in Adaptive computation and machine learning. Cambridge, Mass: MIT Press, 2006.


Author and contact in case of questions: 

Oleksandr Naumenko ([oleksandr.naumenko@cern.ch](mailto:oleksandr.naumenko@cern.ch))

## Package Requirements
- pyyaml
- numpy
- scikit-optimize
- matplotlib
- scipy
- h5py
- blond (Used for the longitudinal beam dynamics simulations, see [here](https://gitlab.cern.ch/blond/BLonD))
- PSImpedance (PS longitudinal impedance model for the simulations, see [here](https://gitlab.cern.ch/longitudinal-impedance/PS/))

## Installation
Install by running 
  
  `pip install git+https://gitlab.cern.ch/onaumenk/gaussianoptimizer`, 
  
  or cloning the repository and running 
  
  `pip install -e.` inside the project directory.

  Note that the optimizer uses HTCondor to efficiently run the simulations. Therefore, this package should be installed in your `/afs/` directory, to have access to HTCondor. 
   


## Contents
- `blond_additions`: Custom-modified functions from BlonD used to run the simulation accurately
- `condor_submission`: Scripts to allow the optimizer to interface and dynamically submit the simulations to HTCondor. See the [project gitlab](https://gitlab.cern.ch/onaumenk/dynamic_condor_submission) for details.
- `data_processing`: Functions that process simulation data to obtain e.g. bunch profiles and evaluate quality metrics to define a Loss function for the optimizer
- `optimizer`: Contains the script for the actual simulation `PS_EASTBIGTOF_optimizer.py`, and the script used to initialze the optimizer on HTCondor `run_optimizer.py`


## Getting Started

To run the optimizer as it is, adjust the global parameters (written in ALLCAPS) defining simulation in- and output in `run_optimizer.py` to your liking and execute `python run_optimizer.py`. 

Please note that, since the simulations are submitted to HTCondor, you must specify which python environment to use in the bashrc file you pass to HTCondor. Therefore, make sure that the passed bashrc file and the parameter `BASHRC` specifying the path to that passed bashrc in `run_optimizer.py` are setup correctly.

## Detailed Adjustments

For further adjustments, `PS_EASTBIGTOF_optimizer.py` and `run_optimizer.py` must be modified. 

  In `run_optimizer.py`:
  1. The global variables in ALLCAPS must be modified to adjust the parameters passed to HTCondor, e.g. the name of the mainfile, the runtime of the simulations, etc.
  2. The loss function must be modified to calculate the loss with the desired method and based on the chosen figures of merit. For this, both the calculation of the loss function itself, and the script to obtain the figures of merit used in the loss function, must be modified/rewritten.
  3. The parameters the optimizer scans must be defined at the bottom of the file. For that, both their keyword argument names in the given mainfile (e.g. if your mainfile has a run function `run(..., param_1 = value)` then `"param_1"` is the name of that parameter), and the minimum/maximum value of each parameter must be given.
  4. The Optimizer definition itself can be adjusted at the bottom of the file. This includes the kernel of the Gaussian process, noise levels and acquisition function, among others. See the definition of [Gaussian Process Regressors](https://scikit-optimize.github.io/stable/modules/generated/skopt.learning.GaussianProcessRegressor.html) and [Optimizers](https://scikit-optimize.github.io/stable/modules/generated/skopt.Optimizer.html?highlight=optimizer#skopt.Optimizer) in scikit-optimize for details.
   

The file definining the actual simulations, `PS_EASTBIGTOF_optimizer.py`, may be adjusted to simulate wholly different scenarios. However, there are **2 hard-coded requirements**:

1. The function running the simulation must be called `run(...)`.
2. The `run()` function must have a keyword argument `run(..., output_dir = some_path)` defining where output data necessary to evaluate the loss of simulations is stored. This directory must be created and populated with data by the `run(...)` function.

For further details, please view the documentation inside the files there. 


## Implemented Example

As is, the optimizer in the folder `optimizer`, optimizes the phase jump settings for the PS "parasitic" TOF cycle with EAST.

Concretely, the cylce contains 2 bunches: EAST and TOF. The TOF bunch is extracted
during an intermediate flat top at 729 ms cycle time, after which the EAST bunch continues to accelerate and is later extracted. The simulation only runs from 726 to 735 ms cycle time, and does not include EAST extraction. What it includes is:

- The rotation of TOF bunch, which is done by the first phase jump, which also rotates the EAST bunch.
- The subsequent TOF bunch extraction.
- The second rotation by phase jump of the EAST bunch afterwards, which attempts to compensate the effects of the first phase jump on the EAST bunch. ("counter-rotation)
  
The parameter space optimized is:

1. the amplitude of the 1st jump towards the unstable phase
2. the amplitude of the 1st jump back from the unstable phase to the stable phase
3. the start time of the 1st jump
4. the duration of the 1st jump
5. the start time of the 2nd jump
6. the duration of the 2nd jump

To study the workings of the optimizer, look into `run_optimizer.py` for the 3  parts necessary for the optimization process:

1. The loss function. It takes into account TOF bunch and tail length at extraction, residual EAST quadrupole oscillation amplitude after TOF extraction, and particle losses in both bunches.
2. The method to evaluate the loss function for given parameters through running the simulations.
3. The actual optimizer, using Gaussian Processes, implemented with the `scikit-optimize` package.


## List of BLonD Modifications

- `beam_feedback_abc.py` and `blond_PS_llrf.py`: A class-based implementation of the BLonD beam feedback framework. Currently implements the 2023 model of the H8H16 beam feedback loop control of the PS. Implements the `BeamFeedback_PS` class to calculate and adjust the RF phase based on phase and radial loops.
- `blond_beam.py` and `blond_separatrix.py`: Custom beam loss calculations via the `CustomBeam` class, based on a separatrix that is shifted due to RF frequency shifts (induced by the above-mentioned feedback loops). Note that this shift might cause problems on turns where the RF frequency is abruptly and strongly changed, e.g. right after TOF extraction. This might cause faulty loss calculations.
  The class automatically calculates losses when no phase jump is being performed, and allows taking into account "fake" acceleration not present in the simulation to take into account e.g. losses due to acceleration after an energy plateau, eventhough the simulation only covers the energy plateau.
- `blond_comoving_profile.py`: A profile moving together with RF frequency shifts, mimicking how actual measurements are based of the clocks/timings of RF systems. E.g. will produce shifts in phase when the RF frequency shifts strongly, as seen in measurements during TOF extraction in parasitic TOF cycles at the PS.
- `blond_monitors.py` Implements both a `MultiBunchMonitor` and `LossesMonitor`, with the former saving the beam profile data to a `profile.h5` file in the output directory. The latter is designed specifically to track losses in a parasitic TOF cycle with an EAST bunch, using the `CustomBeam` class implemented in `blond_beam.py`.
- `blond_plot_beams.py` and `blond_plot.py`: Slight adjustments to obtain nice phase space plots, with the area particles are considered captured in being marked, and the separatrix being shifted according to the shift calculated with `blond_separatrix.py`.